import os
from translate import Translate


class Scaner:
    extensions = ['vtt', 'srt']

    def scan(self):
        path = '/mnt/store/learning/Ultimate Unity Overview (30+ Tools and Features Explained!)'
        os.listdir()
        for root, dirs, files in os.walk(path):
            for filename in files:
                fullname = os.path.join(root, filename)
                filename_split = filename.split('.')
                if filename_split[-1:][0] not in self.extensions:
                    continue
                print(fullname)
                new_filename = filename.replace('.en', ' en')#.replace('English', 'en').replace('Russian', 'ru')
                os.rename(fullname, os.path.join(root, new_filename))


if __name__ == '__main__':
    scaner = Scaner()
    scaner.scan()
