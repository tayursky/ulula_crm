import requests
import pprint


class Translate:
    yandexPassportOauthToken = 'y0_AgAAAAAYtqUcAATuwQAAAADkrgVmzE07DLdcRNS6nh3qZH52sqLS_SY'
    folder_id = 'b1glfkieivi3k1kl5qg8'
    iam_token = None
    expires_at = None
    target_language = 'ru'

    def __init__(self):
        if not self.iam_token:
            self.get_iam_token()
        # print(self.translate(['escape from earth', 'mars']))

    def get_iam_token(self):
        response = requests.post(
            'https://iam.api.cloud.yandex.net/iam/v1/tokens',
            json={'yandexPassportOauthToken': self.yandexPassportOauthToken}
        )
        self.iam_token = response.json().get('iamToken')
        self.expires_at = response.json().get('expiresAt')
        pprint.pprint(response.json())

    def translate(self, text):
        headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer {0}'.format(self.iam_token)}
        body = {
            'folderId': self.folder_id,
            'targetLanguageCode': self.target_language,
            'texts': text,
        }
        response = requests.post(
            'https://translate.api.cloud.yandex.net/translate/v2/translate', json=body, headers=headers
        )
        pprint.pprint(response.json())
        return response.json()['translations'][0].get('text')


if __name__ == '__main__':
    translate = Translate()
