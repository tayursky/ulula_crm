import os
from translate import Translate


class Scaner:
    path = '/media/roman/purple/tutorial/Ultimate Unity Overview (30+ Tools and Features Explained!)/[TutsNode.com] - Ultimate Unity Overview (30+ Tools and Features Explained!)'
    extensions = ['vtt', 'srt']
    translate = None

    def __init__(self):
        self.translate = Translate()

    def scan(self):
        os.listdir()
        for root, dirs, files in os.walk(self.path):
            for filename in files:
                fullname = os.path.join(root, filename)
                extension = filename.split('.')[-1:][0]
                if extension not in self.extensions:
                    continue
                filename_title = filename.split('.')[0]
                language = filename_title.split(' ')[-1:][0]
                if language != 'en':
                    continue

                filename_ru = ' '.join(filename_title.split(' ')[:-1] + ['ru']) + '.' + extension
                print(language, filename_ru)

                vtt_ru_fullname = os.path.join(root, filename_ru)
                vtt_ru = open(vtt_ru_fullname, 'w+')
                vtt_en = open(fullname, 'r')
                for english_line in vtt_en.readlines():
                    value = english_line.strip()
                    try:
                        value = int(value)
                        vtt_ru.write('%s\n' % value)
                    except ValueError:
                        if not value:
                            continue
                        elif value[13:16] == '-->':
                            vtt_ru.write('%s\n' % value)
                        else:
                            # vtt_ru.write('%s\n\n' % value)
                            vtt_ru.write('%s\n\n' % self.translate.translate(value))
                vtt_ru.close()
                vtt_en.close()


if __name__ == '__main__':
    scaner = Scaner()
    scaner.scan()
