# pip install -r ./requirements.txt

django==4.1.7
django-debug-toolbar==2.2
django-cors-headers==3.14.0
django-safedelete==1.3.1
django-silk==5.0.3
django-simple-history==3.3.0
djangorestframework==3.14
django-filter==23.1
#drf-nested-routers==0.93.4

ipdb==0.13.13
psycopg2-binary==2.8.6
python-dotenv==1.0.0
requests==2.24.0
lxml==4.9.2
bs4==0.0.1
russian-names==0.1.2
