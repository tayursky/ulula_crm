import calendar
from datetime import date, datetime, timedelta

from django.utils.translation import gettext_lazy as _
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status
from django_filters import rest_framework as filters

from apps.appoint.filters import AppointFilter
from apps.appoint.models import Appoint, Stage
from apps.appoint.serializers import AppointListSerializer, AppointDetailSerializer, AppointInputSerializer
from apps.company.filters import TimetableFilter
from apps.company.models import Branch, Timetable
from apps.directory.models import WorkingCalendar
from common.views import CommonModelViewSet
from identity.serializers import PersonSerializer, ContactSerializer


class AppointViewSet(CommonModelViewSet):
    label = 'appoint'
    model = Appoint
    serializer_class = AppointListSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = AppointFilter
    branch, timezone = None, 0
    month, day = None, None
    begin, end = None, None

    serializer_class_by_action = dict(
        form=AppointDetailSerializer,
        retrieve=AppointDetailSerializer,
        create=AppointInputSerializer,
        update=AppointInputSerializer
    )

    def dispatch(self, request, *args, **kwargs):
        print('dispatch', self.serializer_class)
        return super().dispatch(request, *args, **kwargs)

    def get_serializer_class(self):
        if hasattr(self, 'serializer_class_by_action'):
            return self.serializer_class_by_action.get(self.action, self.serializer_class)
        return super().get_serializer_class()

    def get_queryset(self):
        qs = self.model.objects.filter(status__in=['in_work', 'done']).exclude(stage__name='cancel') \
            .select_related('branch', 'stage').prefetch_related('services', 'persons').order_by('begin')
        return self.filter_queryset(qs)

    def get_timetable_queryset(self):
        timetable_qs = Timetable.objects.filter(staff__user__groups__name__in=['Специалисты']) \
            .select_related('branch', 'branch__city', 'staff').order_by('begin')
        timetable_filter = TimetableFilter(self.request.GET, request=self.request.GET, queryset=timetable_qs)
        timetable_filter.is_valid()
        return timetable_filter.filter_queryset(timetable_qs)

    @action(detail=False, methods=['get'])
    def case(self, request, *args, **kwargs):
        result = self.meta_dict()
        result['filters'] = self.filterset_class().widget_filters(request)
        return Response(result)

    @action(detail=False, methods=['get'])
    def form(self, request, *args, **kwargs):
        self.serializer_class = self.get_serializer_class()
        return Response(dict(
            appoint_form=self.form_dict(),
            person_form=PersonSerializer().form_dict(),
            contact_form=ContactSerializer().form_dict()
        ), status=status.HTTP_200_OK)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        # super().create(self, request, *args, **kwargs)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        print('validated_data', validated_data)
        appoint = serializer.save()
        for person in serializer.persons:
            print(person)
            appoint.persons.add(person['person'], through_defaults=dict(role=person['role']))
        appoint.stage_id = 4
        appoint.save()
        print('persons', serializer.persons)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        # super().update(self, request, *args, **kwargs)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        # print('end', serializer.instance.end)
        # import ipdb; ipdb.set_trace()
        # for person in serializer.persons:
        #     print(person)
        #     appoint.persons.add(person['person'], through_defaults=dict(role=person['role']))
        # appoint.stage_id = 4
        # appoint.save()
        # print('persons', serializer.persons)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        # super().destroy(self, request, *args, **kwargs)
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_200_OK)
