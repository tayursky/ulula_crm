import calendar
from datetime import date, datetime, timedelta

from django.utils.translation import gettext_lazy as _
from rest_framework.response import Response

from apps.appoint.views import AppointViewSet
from apps.appoint.models import Stage
from apps.appoint.serializers import StageSerializer
from apps.directory.models import WorkingCalendar


class AppointMonthViewSet(AppointViewSet):
    """
        Расписание на месяц
    """
    month = None  # 2020-01

    def dispatch(self, request, *args, **kwargs):
        self.set_range()
        return super().dispatch(request, *args, **kwargs)

    def set_range(self):
        try:
            self.month = datetime.strptime(self.request.GET.get('month') + '-1', '%Y-%m-%d').date()
        except (TypeError, ValueError):
            self.month = date.today()
        if self.request.GET.get('step') == 'previous':
            self.month -= timedelta(days=1)
        elif self.request.GET.get('step') == 'next':
            self.month += timedelta(days=33)
        self.month = self.month.replace(day=1)

        begin = datetime.combine(self.month, datetime.min.time()) + timedelta(hours=self.timezone)
        end = begin.replace(day=calendar.monthrange(self.month.year, self.month.month)[1])
        end = datetime.combine(end, datetime.max.time()) + timedelta(hours=self.timezone)
        self.request.GET._mutable = True
        self.request.GET.update(begin=begin, end=end)

    def list(self, request, *args, **kwargs):
        stages_data = StageSerializer(Stage.objects.all(), many=True).data
        stages = {stage['name']: 0 for stage in stages_data}
        appoints = dict()
        branches = dict()

        for timetable in self.get_timetable_queryset() \
                .values('branch_id', 'branch__name', 'branch__city__name', 'begin'):
            day_string = timetable['begin'].strftime('%Y-%m-%d')
            branch_id = timetable['branch_id']
            if day_string not in appoints.keys():
                appoints[day_string] = {}
            if branch_id not in appoints[day_string].keys():
                appoints[day_string][branch_id] = stages.copy()
            if branch_id not in branches.keys():
                branches[branch_id] = f"{timetable['branch__city__name']}, {timetable['branch__name']}"

        values = ['branch_id', 'branch__name', 'branch__city__name', 'begin', 'stage__name']
        appoint_qs = self.get_queryset().values(*values)
        for _appoint in appoint_qs:
            day_string = _appoint['begin'].strftime('%Y-%m-%d')
            branch_id = _appoint['branch_id']
            if day_string not in appoints.keys():
                appoints[day_string] = {}
            if branch_id not in appoints[day_string].keys():
                appoints[day_string][branch_id] = stages.copy()
            if branch_id not in branches.keys():
                branches[branch_id] = f"{_appoint['branch__city__name']}, {_appoint['branch__name']}"
            appoints[day_string][branch_id][_appoint['stage__name']] += 1

        return Response(dict(
            month=self.month.strftime('%Y-%m'),
            month_name=_(self.month.strftime('%B')),
            timesheet=WorkingCalendar.get_timesheet(year=self.month.year, month=self.month.month),
            appoints=appoints,
            branches=branches,
            stages=stages_data,
        ))
