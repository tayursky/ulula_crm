import calendar
from datetime import date, datetime, timedelta

from django.utils.translation import gettext_lazy as _
from rest_framework.response import Response

from apps.appoint.views import AppointViewSet
from apps.appoint.models import Stage, AppointPerson
from apps.company.models import Branch, Service
from identity.models import Person
from apps.company.serializers import TimetableSerializer
from apps.directory.models import WorkingCalendar


class TimesheetWeekViewSet(AppointViewSet):
    """
        Расписание на неделю
    """

    def dispatch(self, request, *args, **kwargs):
        self.branch = Branch.objects.get(pk=request.GET.get('branch', 1))
        self.set_range()
        return super().dispatch(request, *args, **kwargs)

    def set_range(self):
        range_days = int(self.request.GET.get('range', 7))
        try:
            self.day = datetime.strptime(self.request.GET.get('day'), '%Y-%m-%d').date()
        except (TypeError, ValueError):
            self.day = date.today()
        if self.request.GET.get('step') == 'previous':
            self.day -= timedelta(days=range_days)
        elif self.request.GET.get('step') == 'next':
            self.day += timedelta(days=range_days)
        if range_days == 7:
            self.day -= timedelta(days=calendar.weekday(self.day.year, self.day.month, self.day.day))

        self.begin = datetime.combine(self.day, datetime.min.time())
        self.end = datetime.combine(self.begin + timedelta(days=range_days - 1), datetime.max.time())
        self.request.GET._mutable = True
        self.request.GET.update(begin=self.begin, end=self.end)

    def list(self, request, *args, **kwargs):
        appoints = self.serializer_class(self.get_queryset(), many=True).data
        timetable_qs = self.get_timetable_queryset()
        timetable = TimetableSerializer(timetable_qs, many=True).data
        timesheet = WorkingCalendar.get_timesheet(begin=self.begin, end=self.end, only_days=True)
        timesheet['begin_time'] = self.branch.begin_time.strftime('%H:%M')
        timesheet['end_time'] = self.branch.end_time.strftime('%H:%M')
        related = self.get_related(timetable, appoints)
        return Response(dict(
            branch=self.branch.id,
            day=self.day.strftime('%Y-%m-%d'),
            week_label=self.get_week_label(self.begin, self.end),
            timesheet=timesheet,
            timetable=timetable,
            appoints=appoints,
            related=related,
        ))

    def get_related(self, timetable, appoints):
        related = dict(
            client=[],
            staff=[],
            service=[],
            stage=Stage.objects.all().values('id', 'name', 'color', 'color_hover', 'color_bg', 'color_bg_hover')
        )
        for a_p in AppointPerson.objects.filter(role='client', appoint_id__in=[i['id'] for i in appoints]) \
                .select_related('person'):
            related['client'].append(dict(
                id=a_p.person.id,
                full_name=a_p.person.full_name,
                last_name=a_p.person.last_name,
                initials=a_p.person.initials,
                age=a_p.person.age
            ))
        for staff in Person.objects.filter(pk__in=set([i['staff'] for i in timetable])) \
                .select_related('user'):
            related['staff'].append(dict(
                id=staff.id,
                full_name=staff.full_name,
                last_name=staff.last_name,
                initials=staff.initials
            ))
        for service in Service.objects.all().order_by('name'):
            related['service'].append(dict(
                id=service.id,
                name=service.name,
                comment=service.comment
            ))
        return related

    @staticmethod
    def get_week_label(begin, end):
        label = '%s %s %s' % (begin.day, _(begin.strftime('%B')), begin.year)
        if begin.day != end.day:
            label = str(begin.day)
            if begin.month != end.month:
                label += ' %s' % _(begin.strftime('%B')).lower()
            if begin.year != end.year:
                label += ' %s' % begin.year
            label += ' - %s %s %s' % (end.day, _(end.strftime('%B')).lower(), end.year)
        return label
