from django.core.management.base import BaseCommand, CommandError

from russian_names import RussianNames
from apps.appoint.models import Appoint
from identity.models import Person


class Command(BaseCommand):
    help = 'make appoints faking'

    def handle(self, *args, **options):
        person_qs = Person.objects.all()
        rn = RussianNames(count=person_qs.count())
        batch = rn.get_batch()
        for index, person in enumerate(Person.objects.all()):
            person.first_name, person.patronymic, person.last_name = batch[index].split(' ')
            person.save()
            print(index, person)
