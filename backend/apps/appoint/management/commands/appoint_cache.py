from django.core.management.base import BaseCommand, CommandError

from apps.appoint.models import Appoint


class Command(BaseCommand):
    help = 'make appoints cache'

    def handle(self, *args, **options):
        qs = Appoint.objects.all()
        count = qs.count()
        for appoint in qs:
            print(count)
            count -= 1
            appoint.save()
            print(appoint, appoint.cache)
