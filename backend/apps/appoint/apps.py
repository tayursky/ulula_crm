from django.apps import AppConfig


class AppointConfig(AppConfig):
    name = 'apps.appoint'
