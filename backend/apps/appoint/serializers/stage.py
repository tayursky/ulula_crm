from rest_framework import serializers

from apps.appoint.models import Stage


class StageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stage
        ordering = ['step']
        fields = ['id', 'step', 'name', 'label', 'color', 'color_hover', 'color_bg', 'color_bg_hover', 'comment']
        headers = ['step', 'name', 'label']
        form_fields = ['step', 'name', 'label', 'color', 'color_hover', 'color_bg', 'color_bg_hover', 'comment']
        ordering_fields = ['step', 'name']
