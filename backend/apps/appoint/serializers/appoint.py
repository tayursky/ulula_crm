from datetime import datetime, timedelta

from rest_framework import serializers

from common.serializers import CommonModelSerializer
from apps.appoint.models import Appoint
from apps.company.models import Branch
from identity.models import Person
from apps.company.serializers import BranchSerializer
from identity.serializers import PersonSerializer


class AppointListSerializer(CommonModelSerializer):
    begin = serializers.DateTimeField(source='begin_tz', read_only=True)
    end = serializers.DateTimeField(source='end_tz', read_only=True)

    class Meta:
        model = Appoint
        fields = [
            'id', 'branch', 'services', 'status', 'stage', 'begin', 'end', 'minutes',
            'cost', 'discount', 'paid', 'paid_non_cash', 'comment', 'cache'
        ]


class AppointDetailSerializer(CommonModelSerializer):
    begin = serializers.DateTimeField(source='begin_tz', read_only=True, label='Начало приема')
    end = serializers.DateTimeField(source='end_tz', read_only=True, label='Окончание приема')
    # persons = PersonSerializer(many=True)
    clients = serializers.SerializerMethodField()

    prefetch_fields = ['persons']

    class Meta:
        model = Appoint
        fields = [
            'id', 'branch', 'services', 'status', 'stage', 'begin', 'end', 'minutes',
            'cost', 'discount', 'paid', 'paid_non_cash', 'comment', 'cache',
            'clients'
        ]
        form_fields = [
            'status', 'stage', 'branch', 'services', 'begin', 'end',
            'cost', 'discount', 'paid', 'paid_non_cash', 'comment'
        ]
        widgets = dict(
            services=dict(choices='__all__')
        )

    def get_clients(self, obj):
        qs = obj.persons.filter(appoints__appoint=obj, appoints__role='client')
        return PersonSerializer(qs, many=True).data


class AppointInputSerializer(CommonModelSerializer):

    class Meta:
        model = Appoint
        fields = [
            'id', 'branch', 'services', 'status', 'stage', 'begin', 'end', 'minutes',
            'cost', 'discount', 'paid', 'paid_non_cash', 'comment', 'cache',
            'persons'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.branch = Branch.objects.get(pk=self.initial_data.get('branch'))
        self.timezone = self.branch.city.timezone
        try:
            self.begin_datetime = datetime.strptime(self.initial_data.get('begin'), '%Y-%m-%dT%H:%M:%S')
            self.begin_datetime -= timedelta(hours=self.timezone)
        except TypeError:
            self.begin_datetime = None
        try:
            self.end_datetime = datetime.strptime(self.initial_data.get('end'), '%Y-%m-%dT%H:%M:%S')
            self.end_datetime -= timedelta(hours=self.timezone)
        except TypeError:
            self.end_datetime = None
        self.persons = []
        try:
            self.staff = Person.objects.get(pk=self.initial_data.get('staff'))
            self.persons.append(dict(person=self.initial_data.get('staff'), role='staff'))
        except (TypeError, Person.DoesNotExist):
            self.staff = None

    # def validate(self, data):
    #     # import ipdb; ipdb.set_trace()
    #     # print('validate')
    #     # instance = Appoint(**data)
    #     # instance.clean()
    #     return data

    def validate_branch(self, value):
        if not value and (self.initial_data.get('begin') or self.initial_data.get('end')):
            raise serializers.ValidationError('Необходимо выбрать филиал')
        return value

    def validate_begin(self, value):
        # import ipdb; ipdb.set_trace()
        if not value and self.initial_data.get('end'):
            raise serializers.ValidationError('Необходимо время начала')
        if value and not self.initial_data.get('end'):
            raise serializers.ValidationError('Необходимо время окончания')
        if self.begin_datetime and self.end_datetime and self.begin_datetime >= self.end_datetime:
            raise serializers.ValidationError('Время начала приема должно быть больше окончания')
        return self.begin_datetime

    def validate_end(self, value):
        # if not self.initial_data.get('begin'):
        #     raise serializers.ValidationError('Необходимо время начала')
        print('validate_end', self.end_datetime)
        return self.end_datetime

    def validate_persons(self, value):
        data = list()
        if self.staff:
            data.append(dict(person=self.staff, role='staff'))
        return data

    def update(self, instance, validated_data):
        super().update(instance, validated_data)
        # import ipdb; ipdb.set_trace()
        for client_data in self.initial_data.get('clients'):
            client = PersonSerializer(data=client_data)
            if client.is_valid():
                client.save()
            print(client.errors)
        return instance