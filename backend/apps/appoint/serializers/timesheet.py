from rest_framework import serializers

from apps.appoint.models import Appoint


class TimeSheetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appoint
        fields = [
            'id', 'branch', 'status', 'stage', 'services', 'begin', 'end',
            'cost', 'discount', 'paid', 'paid_non_cash', 'comment', 'manager', 'specialist'
        ]
        headers = ['branch', 'status', 'stage']
        form_fields = [
            'branch', 'status', 'stage', 'services', 'begin', 'end',
            'cost', 'discount', 'paid', 'paid_non_cash', 'comment', 'manager', 'specialist'
        ]
