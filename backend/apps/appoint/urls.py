from django.urls import path, include
from rest_framework import routers

from .views import AppointViewSet, AppointMonthViewSet, TimesheetWeekViewSet

router = routers.DefaultRouter()
router.register('month', AppointMonthViewSet, basename='appoint_month')
router.register('week', TimesheetWeekViewSet, basename='appoint_week')
router.register('', AppointViewSet, basename='appoint_main')
urlpatterns = [
    path('', include(router.urls)),
]
urlpatterns += router.urls
