# Generated by Django 4.1.7 on 2023-08-28 17:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appoint', '0013_alter_appoint_branch'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='appointperson',
            options={'default_permissions': (), 'verbose_name': 'Прием: персона', 'verbose_name_plural': 'Прием: персоны'},
        ),
        migrations.AlterModelOptions(
            name='historicalappointperson',
            options={'get_latest_by': ('history_date', 'history_id'), 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical Прием: персона', 'verbose_name_plural': 'historical Прием: персоны'},
        ),
    ]
