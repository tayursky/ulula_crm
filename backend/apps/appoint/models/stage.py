from django.db import models
from simple_history.models import HistoricalRecords

from common.models import BaseModel


class Stage(BaseModel):
    """
        Этапы приёмов
    """
    step = models.PositiveSmallIntegerField('Последовательность', default=0)
    name = models.CharField('Системное имя', max_length=32)
    label = models.CharField('Наименование', max_length=32)
    color = models.CharField('Цвет текста', max_length=7, default='#000000')
    color_hover = models.CharField('Цвет фона (hover)', max_length=7, default='#000000')
    color_bg = models.CharField('Цвет фона', max_length=7, default='#ffffff')
    color_bg_hover = models.CharField('Цвет фона (hover)', max_length=7, default='#ffffff')
    comment = models.TextField('Комментарий', null=True, blank=True)
    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Этап сделки'
        verbose_name_plural = 'Этапы сделок'
        ordering = ['step']
        default_permissions = ()
        permissions = [
            ('add_stage', 'Добавлять этапы сделки'),
            ('change_stage', 'Редактировать этапы сделки'),
            ('delete_stage', 'Удалять этапы сделки'),
            ('view_stage', 'Просматривать этапы сделки')
        ]

    def __str__(self):
        return '%s %s' % (self.step, self.label)
