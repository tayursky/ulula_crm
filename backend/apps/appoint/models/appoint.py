from datetime import datetime, timedelta

from django.core.exceptions import ValidationError
from django.db import models
from simple_history.models import HistoricalRecords

from common.models import BaseModel


class Appoint(BaseModel):
    """
        Приём
    """
    STATUSES = (
        ('in_work', 'В работе'),
        ('done', 'Завершено'),
        ('cancelled', 'Отменено')
    )
    branch = models.ForeignKey('company.Branch', null=True, on_delete=models.CASCADE, verbose_name='Филиал')
    status = models.CharField(max_length=9, choices=STATUSES, default='in_work', verbose_name='Статус')
    stage = models.ForeignKey('appoint.Stage', null=True, on_delete=models.CASCADE, verbose_name='Этап')
    services = models.ManyToManyField('company.Service', blank=True, verbose_name='Услуги')

    begin = models.DateTimeField(null=True, verbose_name='Начало приёма')
    end = models.DateTimeField(null=True, verbose_name='Конец приёма')

    cost = models.DecimalField(default='0.00', max_digits=30, decimal_places=2, verbose_name='Стоимость')
    discount = models.BooleanField(default=False, verbose_name='Скидка')
    paid = models.DecimalField(default='0.00', max_digits=30, decimal_places=2, verbose_name='Оплачено')
    paid_non_cash = models.DecimalField(default='0.00', verbose_name='Оплачено безналом',
                                        max_digits=30, decimal_places=2)
    persons = models.ManyToManyField(
        'identity.Person', through='appoint.AppointPerson', through_fields=('appoint', 'person')
    )
    comment = models.TextField(blank=True, verbose_name='Комментарий')
    cache = models.JSONField(default=dict)

    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Прием'
        verbose_name_plural = 'Приемы'
        ordering = ['-begin']
        default_permissions = ()
        permissions = [
            ('add_appoint', 'Добавлять приемы'),
            ('change_appoint', 'Редактировать приемы'),
            ('delete_appoint', 'Удалять приемы'),
            ('view_appoint', 'Просматривать приемы')
        ]

    def __str__(self):
        return f'{self.branch} {self.stage}'

    def cache_update(self):
        cache = dict(
            timezone=self.branch.city.timezone if self.branch and self.branch.city else 0
        )
        for through in AppointPerson.objects.filter(appoint=self):
            if through.role not in cache:
                cache[through.role] = []
            cache[through.role].append(through.person_id)
        self.cache = cache
        return cache

    @property
    def timezone(self):
        return self.cache.get('timezone', 0)

    @property
    def begin_tz(self):
        return self.begin + timedelta(hours=self.timezone) if self.begin else self.begin

    @property
    def end_tz(self):
        return self.end + timedelta(hours=self.timezone) if self.end else self.end

    @property
    def minutes(self):
        return int((self.end - self.begin).seconds / 60) if self.end and self.begin else 0

    def clean(self):
        if self.begin and self.end and self.begin >= self.end:
            raise ValidationError('End date must be after begin date')

    def save(self, *args, **kwargs):
        self.cache_update()
        super().save(*args, **kwargs)


class AppointPerson(models.Model):
    """
        Связи сделок с персонами
    """
    ROLE = (
        ('manager', 'Администратор'),
        ('client', 'Клиент'),
        ('staff', 'Специалист'),
    )
    appoint = models.ForeignKey(Appoint, on_delete=models.CASCADE)
    person = models.ForeignKey('identity.Person', related_name='appoints', verbose_name='Персона',
                               on_delete=models.CASCADE)
    role = models.CharField(max_length=128, choices=ROLE, verbose_name='Роль')
    primary = models.BooleanField(default=True, verbose_name='Основной')
    control = models.BooleanField(default=False, verbose_name='Контроль')
    diagnostic = models.BooleanField(default=False, verbose_name='Диагностика')
    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Прием: персона'
        verbose_name_plural = 'Прием: персоны'
        default_permissions = ()
