from django_filters import rest_framework as filters

from utils.remote_widget import RemoteFilters
from apps.appoint.models import Appoint
from apps.company.models import Branch
from apps.company.models import Staff


class AppointFilter(filters.FilterSet, RemoteFilters):
    begin = filters.DateTimeFilter(field_name='begin', lookup_expr='gte')
    end = filters.DateTimeFilter(field_name='end', lookup_expr='lte')
    staff = filters.CharFilter(label='Специалист', method='filter_staff')

    class Meta:
        model = Appoint
        fields = ('branch', 'staff', 'begin', 'end')
        remote = dict(
            branch=dict(
                class_name='PrimaryKeyRelatedField', label='Филиал', qs='get_branch_qs',
            ),
            staff=dict(
                class_name='PrimaryKeyRelatedField', label='Специалист', qs='get_staff_qs',
            ),
        )

    @staticmethod
    def filter_staff(queryset, name, value):
        return queryset.filter(persons=value, appointperson__role='staff')

    @staticmethod
    def get_branch_qs(request):
        qs = Branch.objects.filter(is_active=True).order_by('city', 'name').select_related('city')
        return qs

    @staticmethod
    def get_staff_qs(request):
        qs = Staff.objects.all()
        return qs


class AppointWeekFilter(filters.FilterSet, RemoteFilters):
    staff = filters.CharFilter(label='Специалист', method='filter_staff')

    class Meta:
        model = Appoint
        fields = ['staff']
        remote = dict(
            branch=dict(
                class_name='PrimaryKeyRelatedField', label='Филиал', qs='get_branch_qs',
                widget=dict(
                    class_name='PrimaryKeyRelatedField',
                    search_url='/common/search/',
                    model='company.Branch'
                )),
            staff=dict(
                class_name='PrimaryKeyRelatedField', label='Специалист', qs='get_staff_qs',
                widget=dict(
                    class_name='PrimaryKeyRelatedField',
                    search_url='/common/search/',
                    model='company.Staff'
                ))
        )

    @staticmethod
    def filter_staff(queryset, name, value):
        return queryset.filter(cache__staff=int(value))

    @staticmethod
    def get_branch_qs(request):
        qs = Branch.objects.filter(is_active=True).order_by('city', 'name').select_related('city')
        return qs

    @staticmethod
    def get_staff_qs(request):
        qs = Staff.objects.all()
        return qs
