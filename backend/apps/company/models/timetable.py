from datetime import date, datetime, timedelta, time
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q, Count, Avg, Sum, Min, Max, OuterRef, Subquery, Func, F, Exists, Value, Case, When, \
    CharField, DecimalField, IntegerField
from simple_history.models import HistoricalRecords

from config.settings import DATETIME_FORMAT, TIME_FORMAT
from common.models import BaseModel


class TimetableQuerySet(models.QuerySet):

    def with_tz(self):
        qs = self.all()
        # print(qs.count())
        # import ipdb; ipdb.set_trace()
        return qs


class TimetableManager(models.Manager):

    # def create(self, **kwargs):
    #     kwargs.update(comment='TimetableManager')
    #     return super().create(**kwargs)

    def get_queryset(self):
        return TimetableQuerySet(self.model, using=self._db)  # Important!

    def with_tz(self):
        return self.get_queryset().with_tz()


class Timetable(BaseModel):
    """
        Табель учета рабочего времени
    """
    branch = models.ForeignKey('company.Branch', related_name='timetables', verbose_name='Филиал',
                               on_delete=models.CASCADE)
    staff = models.ForeignKey('company.Staff', related_name='timetables', verbose_name='Сотрудник',
                              on_delete=models.CASCADE)
    begin = models.DateTimeField(verbose_name='Начало смены')
    end = models.DateTimeField(verbose_name='Окончание смены')
    begin_really = models.DateTimeField(null=True, verbose_name='Фактическое начало смены')
    end_really = models.DateTimeField(null=True, verbose_name='Фактическое окончание смены')
    comment = models.TextField(null=True, blank=True, verbose_name='Комментарий')
    cache = models.JSONField(default=dict)

    history = HistoricalRecords()
    objects = TimetableManager()

    class Meta:
        verbose_name, verbose_name_plural = ['Табель учета рабочего времени'] * 2
        ordering = []
        default_permissions = ()
        permissions = [
            ('add_timetable', 'Добавлять табель'),
            ('change_timetable', 'Редактировать табель'),
            ('delete_timetable', 'Удалять табель'),
            ('view_timetable', 'Просматривать табель')
        ]
        icon = 'el-icon-date'

    def __str__(self):
        timezone = self.branch.city.timezone
        _begin = (self.begin_really or self.begin) + timedelta(hours=timezone)
        _end = (self.end_really or self.end) + timedelta(hours=timezone)
        return '{branch}, {staff}: {day} {begin}-{end}'.format(
            branch=self.branch.name,
            staff=self.staff,
            day=_begin.strftime('%d.%m.%Y'),
            begin=_begin.strftime(TIME_FORMAT),
            end=_end.strftime(TIME_FORMAT),
        )

    def cache_update(self):
        self.cache.update(self.get_time())
        return True

    def get_time(self):
        delta = self.end - self.begin
        if delta.seconds / 3600 >= 7.5:  # Если смена превышает 7,5 часов, то по закону час на обед
            delta -= timedelta(hours=1)
        return dict(
            seconds=delta.seconds,
            hours=delta.seconds / 3600,
            hours_string=':'.join(str(delta).split(':')[:2])
        )

    def get_begin_tz(self):
        return self.begin + timedelta(hours=self.branch.city.timezone)

    def clean(self):
        timezone = self.branch.city.timezone
        begin, end = self.begin, self.end
        begin_really, end_really = self.begin_really, self.end_really

        if begin >= end:
            raise ValidationError({'begin': u'Время окончания смены должны быть больше начала'})

        if begin_really and end_really and (begin_really >= end_really):
            raise ValidationError({'begin_really': u'Время окончания смены должны быть больше начала'})

        timetable_qs = Timetable.objects.filter(
            Q(staff=self.staff) &
            ((Q(begin__lte=begin, end__gt=begin) |
              Q(begin__lt=end, end__gt=end)) |
             Q(begin__gte=begin, end__lte=begin))
        ).exclude(pk=self.pk)
        if timetable_qs.exists():
            raise ValidationError({
                'end': u'Уже есть смена: %s, %s' % (timetable_qs[0].id, timetable_qs[0].branch)
            })
        self.begin = begin - timedelta(hours=timezone)
        self.end = end - timedelta(hours=timezone)

        if begin_really and end_really:
            timetable_qs = Timetable.objects.filter(
                Q(staff=self.staff) &
                ((Q(begin_really__lte=begin_really, end_really__gt=begin_really) |
                  Q(begin_really__lt=end_really, end_really__gt=end_really)) |
                 Q(begin_really__gte=begin_really, end_really__lte=begin_really))
            ).exclude(pk=self.pk)
            if timetable_qs.exists():
                raise ValidationError({
                    'end_really': u'Уже есть смена: %s, %s' % (timetable_qs[0].id, timetable_qs[0].branch)
                })
        self.begin_really = begin_really - timedelta(hours=timezone)
        self.end_really = end_really - timedelta(hours=timezone)

    def save(self, *args, **kwargs):
        self.cache_update()
        super().save(*args, **kwargs)


class TimetableGroup(BaseModel):
    """
        Группы для расписания
    """
    branch = models.ForeignKey('company.Branch', related_name='time_groups', verbose_name='Филиал',
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=128, blank=True, verbose_name='Наименование')
    staffs = models.ManyToManyField(to='company.Staff', related_name='time_groups', verbose_name='Сотрудники')
    timeout = models.BooleanField(default=False, verbose_name='Перерыв')
    begin_date = models.DateField(verbose_name='Начало периода (день)')
    end_date = models.DateField(verbose_name='Конец периода (день)')
    begin_time = models.TimeField(verbose_name='Начало интервала (время)')
    end_time = models.TimeField(verbose_name='Конец интервала (время)')

    history = HistoricalRecords()

    parent_rel = 'branch'
    icon = 'el-icon-circle-check'

    class Meta:
        verbose_name = 'Группа для расписания'
        verbose_name_plural = 'Группы для расписания'
        ordering = ['branch', 'begin_time', '-end_time', 'begin_date', '-end_date']
        default_permissions = ()
        permissions = [
            ('add_timegroup', 'Добавлять группы для расписания'),
            ('change_timegroup', 'Редактировать группы для расписания'),
            ('delete_timegroup', 'Удалять группы для расписания'),
            ('view_timegroup', 'Просматривать группы для расписания')
        ]

    def __str__(self):
        begin_date, begin_time = self.begin_date.strftime('%d.%m.%Y'), self.begin_time.strftime('%H:%M')
        end_date, end_time = self.end_date.strftime('%d.%m.%Y'), self.end_time.strftime('%H:%M')
        if self.name:
            return '%s: %s-%s (%s-%s)' % (self.name, begin_date, end_date, begin_time, end_time)
        return '%s-%s (%s-%s)' % (begin_date, end_date, begin_time, end_time)
