from datetime import datetime, time
from decimal import Decimal

from django.db import models
from simple_history.models import HistoricalRecords

from common.models import BaseModel


class ServiceGroup(BaseModel):
    """
        Группы услуг
    """
    name = models.CharField(max_length=124, verbose_name='Наименование')
    comment = models.TextField(null=True, blank=True, verbose_name='Комментарий')

    class Meta:
        verbose_name = 'Группа услуг'
        verbose_name_plural = 'Группы услуг'
        ordering = ['name']
        default_permissions = ()
        permissions = [
            ('add_servicegroup', 'Добавлять группы услуги'),
            ('change_servicegroup', 'Редактировать группы услуги'),
            ('delete_servicegroup', 'Удалять группы услуги'),
            ('view_servicegroup', 'Просматривать группы услуги'),
        ]

    def __str__(self):
        return self.name

    def get_services_display(self):
        return ', '.join([i.name for i in self.services.all()])


class Service(BaseModel):
    """
        Услуги
    """
    group = models.ForeignKey(ServiceGroup, null=True, related_name='services', verbose_name='Группа услуг',
                              on_delete=models.CASCADE)
    name = models.CharField(max_length=124, verbose_name='Наименование')
    cost = models.DecimalField(default='0.00', max_digits=30, decimal_places=2, verbose_name='Стоимость')
    cost_kid = models.DecimalField(default='0.00', max_digits=30, decimal_places=2, verbose_name='Стоимость (детская)')
    time = models.TimeField(null=True, verbose_name='Длительность процедуры')
    staffs = models.ManyToManyField(
        'identity.Person', verbose_name='Специалисты', related_name='services',
        through='company.ServiceExecutor', through_fields=('service', 'staff')
    )
    comment = models.TextField(null=True, blank=True, verbose_name='Комментарий')

    history = HistoricalRecords()

    filters_fields = dict(
        group=dict(
            key='group', label='Группа',
            widget=dict(attrs=dict(), name='Select', input_type='select', model_name='company.ServiceGroup')
        ),
        name=dict(
            key='name__icontains', label='Наименование', widget=dict(
                attrs={}, name="TextInput", input_type="text"
            )
        ),
        # masters=dict(
        #     label='Специалисты', key='masters',
        #     widget=dict(attrs={}, name='Select', input_type="select", model_name='apps.company.Master')
        # ),
    )

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'
        ordering = ['group', 'name']
        default_permissions = ()
        permissions = [
            ('add_service', 'Добавлять услуги'),
            ('change_service', 'Редактировать услуги'),
            ('delete_service', 'Удалять услуги'),
            ('view_service', 'Просматривать услуги'),
        ]

    def __str__(self):
        return self.name
        # return '%s: %s' % (self.group.name, self.name)

    def get_executors_string_display(self):
        return ', '.join([i.__str__() for i in self.executors.all()])

    @staticmethod
    def get_cost(executor, services):
        _data = list()
        for item in ServiceExecutor.objects.filter(executor=executor, service__in=services):
            services.remove(item.service.id)
            _data.append(dict(
                service_id=item.service.id,
                service=item.service.name,
                cost=item.cost or item.service.cost,
                cost_kid=item.cost_kid or item.service.cost_kid,
                diagnostic=item.diagnostic
                # time=item.time or item.service.time
            ))
        for service in Service.objects.filter(pk__in=services):
            if service.id not in _data:
                _data.append(dict(
                    service_id=service.id,
                    service=service.name,
                    cost=service.cost,
                    cost_kid=service.cost_kid,
                    diagnostic=service.diagnostic
                    # time=service.time
                ))
        return _data


class ServiceExecutor(BaseModel):
    """
        Оказываемые специалистом услуги
    """
    service = models.ForeignKey(Service, related_name='related_staffs', verbose_name='Услуга',
                                on_delete=models.CASCADE)
    staff = models.ForeignKey('identity.Person', related_name='related_services', verbose_name='Специалист',
                              on_delete=models.CASCADE)
    cost = models.DecimalField(max_digits=30, decimal_places=2, verbose_name='Стоимость',
                               null=True, blank=True)
    cost_kid = models.DecimalField(max_digits=30, decimal_places=2, verbose_name='Стоимость (детская)',
                                   null=True, blank=True)
    time = models.TimeField(null=True, verbose_name='Длительность процедуры')

    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Оказываемая специалистом услуга'
        verbose_name_plural = 'Оказываемые специалистом услуги'
        unique_together = ['service', 'staff']
        ordering = []
        default_permissions = ()
        permissions = [
            ('add_servicestaff', 'Добавлять услуги специалиста'),
            ('change_servicestaff', 'Редактировать услуги специалиста'),
            ('delete_servicestaff', 'Удалять услуги специалиста'),
            ('view_servicestaff', 'Просматривать услуги специалиста'),
        ]

    def __str__(self):
        return '%s %s' % (self.service.__str__(), self.staff.__str__())
