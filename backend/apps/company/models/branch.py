from datetime import datetime, time

from django.db import models
from simple_history.models import HistoricalRecords

from common.models import BaseModel


class Branch(BaseModel):
    """
        Филиалы
    """
    city = models.ForeignKey('directory.City', on_delete=models.CASCADE, verbose_name='Город')
    name = models.CharField(max_length=128, verbose_name='Наименование')
    phone = models.CharField(max_length=32, null=True, blank=True, verbose_name='Телефон')
    address = models.TextField(null=True, blank=True, verbose_name='Адрес')
    begin_time = models.TimeField(default=time(10, 00), null=True, verbose_name='Начало рабочего дня')
    end_time = models.TimeField(default=time(20, 00), null=True, verbose_name='Конец рабочего дня')
    is_active = models.BooleanField(default=True, verbose_name='Активный')
    cache = models.JSONField(default=dict)
    persons = models.ManyToManyField('identity.Person', through='BranchCrew', through_fields=('branch', 'person'))
    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Филиал'
        verbose_name_plural = 'Филиалы'
        unique_together = ('city', 'name')
        # ordering = ('city', 'name')
        default_permissions = ()
        permissions = [
            ('add_branch', 'Добавлять филиалы'),
            ('change_branch', 'Редактировать филиалы'),
            ('delete_branch', 'Удалять филиалы'),
            ('view_branch', 'Просматривать филиалы'),
        ]

    def __str__(self):
        return self.full_name_display

    def cache_update(self):
        self.cache.update(dict(
            full_name_display=f'{self.city.name}, {self.name}',
            managers=list(self.crew.filter(role='manager').values_list('person_id', flat=True)),
            managers_display=', '.join([i.person.short_name for i in self.crew.filter(role='manager')]),
            staffs=list(self.crew.filter(role='staff').values_list('person_id', flat=True)),
            staffs_display=', '.join([i.person.short_name for i in self.crew.filter(role='staff')]),
        ))

    @property
    def full_name_display(self):
        return self.cache.get('full_name_display') or f'{self.city.name}, {self.name}'

    @property
    def work_time_display(self):
        return '%s - %s' % (self.begin_time.strftime('%H:%M'), self.end_time.strftime('%H:%M'))

    @property
    def managers_display(self):
        return self.cache.get('managers_display')

    @property
    def staffs_display(self):
        return self.cache.get('staffs_display')

    def save(self, *args, **kwargs):
        super().save(force_update=False)
        self.cache_update()
        super().save()



class BranchCrew(models.Model):
    """
        Филиалы: персонал
    """
    ROLE = (
        ('manager', 'Администратор'),
        ('staff', 'Специалист'),
    )
    branch = models.ForeignKey(Branch, related_name='crew', on_delete=models.CASCADE)
    person = models.ForeignKey('identity.Person', related_name='branches', verbose_name='Персона',
                               on_delete=models.CASCADE)
    role = models.CharField(max_length=128, choices=ROLE, verbose_name='Роль')
    history = HistoricalRecords()

    class Meta:
        verbose_name = 'Филиал: персона'
        verbose_name_plural = 'Филиалы: персоны'
        default_permissions = ()
