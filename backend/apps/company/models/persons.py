from django.contrib.auth.models import Group, GroupManager
from django.db import models

from common.models import BaseModel
from identity.models import Person


class StaffManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(
            user__is_staff=True, user__username__in=['burlakovskiy', 'rinkarimoff']
        )


class Staff(Person):
    """
        Сотрудники / пользователи системы
    """
    objects = StaffManager()

    class Meta:
        proxy = True
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'
        default_permissions = ()
        permissions = [
            ('add_staff', 'Добавлять пользователей'),
            ('change_staff', 'Редактировать пользователей'),
            ('delete_staff', 'Удалять пользователей'),
            ('view_staff', 'Просматривать пользователей'),
            ('reward_staff', 'Просматривать вознаграждения сотрудников')
        ]

    def __str__(self):
        return '%s %s %s' % (self.last_name, self.first_name, self.patronymic)

    def get_groups_string_display(self):
        return ', '.join([i.name for i in self.user.groups.all()])

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)


class ManagerManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(
            user__is_staff=True
            # , user__is_active=True
        )


class Manager(Person):
    """
        Организаторы
    """
    objects = ManagerManager()

    class Meta:
        proxy = True
        verbose_name = 'Организатор'
        verbose_name_plural = 'Организаторы'
        default_permissions = ()
        permissions = []


class SpecialistManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(
            user__is_active=True, user__groups__name='Специалисты'
        )


class Specialist(Person):
    """
        Специалисты
    """
    objects = SpecialistManager()

    class Meta:
        proxy = True
        verbose_name = 'Специалист'
        verbose_name_plural = 'Специалисты'
        default_permissions = ()
        permissions = []


class UserGroup(Group, BaseModel):
    """
        Группы доступа
    """
    list_display = ['name']
    list_form_fields = ['name']

    class Meta:
        proxy = True
        verbose_name = 'Группа доступа'
        verbose_name_plural = 'Группы доступа'
        ordering = ['name']
        default_permissions = ()
        permissions = [
            ('add_usergroup', 'Добавлять группы доступа'),
            ('change_usergroup', 'Редактировать группы доступа'),
            ('delete_usergroup', 'Удалять группы доступа'),
            ('view_usergroup', 'Просматривать группы доступа'),
        ]
