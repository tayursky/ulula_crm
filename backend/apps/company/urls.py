from django.urls import path, include
from rest_framework import routers

from .viewsets import BranchViewSet, SpecialistViewSet, TimetableViewSet

router = routers.DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
]
router.register('branch', BranchViewSet, basename='branch')
router.register('specialist', SpecialistViewSet, basename='specialist')
router.register('timetable', TimetableViewSet, basename='timetable')

urlpatterns += router.urls
