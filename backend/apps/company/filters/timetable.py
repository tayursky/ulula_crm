from django_filters import rest_framework as filters

from utils.remote_widget import RemoteFilters
from apps.appoint.models import Appoint
from apps.company.models import Branch, Staff, Timetable


class TimetableFilter(filters.FilterSet, RemoteFilters):
    # day = filters.DateFilter(method='filter_day', label='День')
    begin = filters.DateTimeFilter(field_name='begin', lookup_expr='gte')
    end = filters.DateTimeFilter(field_name='end', lookup_expr='lte')

    class Meta:
        model = Timetable
        fields = ('branch', 'staff', 'begin', 'end')
        # widgets = dict(
        #     branch=dict(label='Филиал', class_name='PrimaryKeyRelatedField', qs='get_branch_qs'),
        #     staff=dict(label='Специалист', class_name='PrimaryKeyRelatedField', qs='get_staff_qs'),
        # )

    @staticmethod
    def get_branch_qs(request):
        qs = Branch.objects.filter(is_active=True).order_by('city', 'name').select_related('city')
        return qs

    @staticmethod
    def get_staff_qs(request):
        qs = Staff.objects.all()
        return qs
