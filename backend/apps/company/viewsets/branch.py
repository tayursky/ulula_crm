from rest_framework import viewsets

from apps.company.models import Branch
from apps.company.serializers import BranchSerializer


class BranchViewSet(viewsets.ModelViewSet):
    """
        ViewSet для филиалов
    """
    model = Branch
    serializer_class = BranchSerializer

    def get_queryset(self):
        qs = self.model.objects.filter(is_active=True)
        return qs
