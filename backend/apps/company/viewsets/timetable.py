import calendar
import json
from datetime import date, datetime, timedelta

from django.db.models import Q, ObjectDoesNotExist
from django.utils.translation import gettext_lazy as _

from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets

from apps.company.models import Branch, Timetable
from apps.company.serializers import StaffSerializer, TimetableSerializer
from apps.directory.models import WorkingCalendar
from utils.choices import filters_choices
from utils.remote_widget import RemoteViewSet


class TimetableViewSet(viewsets.ModelViewSet, RemoteViewSet):
    """
        ViewSet для табеля
    """
    model = Timetable
    serializer_class = TimetableSerializer
    begin, end = None, None
    filters = {}
    branch = None

    def dispatch(self, request, *args, **kwargs):
        self.filters = self.get_filters(request)
        print('self.filters', self.filters)
        self.branch = self.get_branch()
        return super().dispatch(request, *args, **kwargs)

    def get_branch(self):
        try:
            branch = Branch.objects.get(pk=self.filters['data']['branch'])
        except (ValueError, ObjectDoesNotExist):
            branch = Branch.objects.get(pk=1)
        return branch

    def get_staffs(self):
        staffs_qs = self.branch.staffs.all()
        return StaffSerializer(staffs_qs, many=True).data

    def get_filters(self, request):
        filters = dict(
            ordering=['branch'],
            data=json.loads(request.GET.get('filters', '{}'))
        )
        if not filters['data']:  # Собираем filters если его нет в запросе
            filters.update(dict(
                fields=dict(
                    branch=dict(
                        class_name='PrimaryKeyRelatedField', key='branch', label='Филиал',
                        widget=dict(attrs={}, name='Select', input_type='select', model_name='company.Branch')
                    ),
                    staff=dict(
                        class_name='PrimaryKeyRelatedField', key='staffs', label='Сотрудник',
                        widget=dict(attrs={}, name='Select', input_type='select', model_name='company.Staff')
                    ),
                )
            ))
            filters['data'] = {i: None for i in filters['fields'].keys()}
            filters = filters_choices(request, filters, self.model)
        else:
            for key in filters['ordering']:
                filters['data'][key] = filters['data'].get(key)
        return filters

    def get_queryset(self):
        _q = Q()
        if self.begin and self.end:
            _q &= Q(begin__gte=self.begin, end__lte=self.end)
        if self.branch:
            _q &= Q(branch=self.branch)
        return self.model.objects.select_related('branch', 'branch__city').filter(_q).with_tz()

    def get_context(self):
        context = dict(
            request=self.request,
            user=self.request.user,
        )
        return context

    def list(self, request, *args, **kwargs):
        try:
            day = datetime.strptime(request.GET.get('day'), '%Y-%m-%d').date()
        except (TypeError, ValueError):
            day = date.today()
        day = day.replace(day=1)
        if request.GET.get('step') == 'prev':
            day = (day - timedelta(days=1)).replace(day=1)
        elif request.GET.get('step') == 'next':
            day = (day + timedelta(days=33)).replace(day=1)

        self.begin = datetime.combine(day, datetime.min.time())
        self.end = self.begin.replace(day=calendar.monthrange(day.year, day.month)[1])
        self.end = datetime.combine(self.end, datetime.max.time())

        self.filters['data']['branch'] = 1
        return Response(dict(
            filters=self.filters,
            day=day.strftime('%Y-%m-%d'),
            month_display='{0} {1}'.format(_(day.strftime('%B')), day.year),
            staffs=self.get_staffs(),
            timesheet=WorkingCalendar.get_timesheet(year=day.year, month=day.month, only_days=True),
            timetable=self.serializer_class(self.get_queryset(), many=True).data,
        ))

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        context = dict(
            form=self.form_dict(),
            data=self.get_serializer(instance).data
        )
        return Response(context)

    @action(detail=False, methods=['POST'])
    def make(self, request):
        return Response(dict(
            form=self.form_dict(),
            data=self.get_serializer().data
        ))
