from rest_framework import viewsets

from apps.company.models import Specialist
from apps.company.serializers import StaffSerializer


class SpecialistViewSet(viewsets.ModelViewSet):
    """
        ViewSet для специалистов
    """
    model = Specialist
    serializer_class = StaffSerializer

    def get_queryset(self):
        qs = self.model.objects.filter(user__is_active=True, user__groups__name='Специалисты')
        return qs
