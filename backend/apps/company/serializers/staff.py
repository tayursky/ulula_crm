from rest_framework import serializers

from apps.company.models import Staff


class StaffSerializer(serializers.ModelSerializer):
    full_name = serializers.ReadOnlyField()
    short_name = serializers.ReadOnlyField()

    class Meta:
        model = Staff
        fields = [
            'id', 'full_name', 'short_name', 'first_name', 'patronymic', 'last_name',
            'birthday', 'timezone', 'comment',
            'address', 'passport_number', 'passport_issued'
        ]
        headers = [
            'id', dict(value='full_name', label='Ф. И. О.')
        ]
        form_fields = [
            'first_name', 'patronymic', 'last_name',
            'birthday', 'timezone', 'comment', 'address', 'passport_number', 'passport_issued'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
