from rest_framework import serializers

from apps.company.models import Branch


class BranchSerializer(serializers.ModelSerializer):
    city = serializers.SerializerMethodField(label='Город')
    work_time = serializers.CharField(source='work_time_display', label='Рабочие часы', read_only=True)
    managers = serializers.CharField(source='managers_display', label='Администраторы', read_only=True)
    staffs = serializers.CharField(source='staffs_display', label='Специалисты', read_only=True)

    class Meta:
        model = Branch
        fields = ['id', 'city', 'name', 'address', 'phone', 'work_time', 'managers', 'staffs']
        select_related = []
        prefetch_related = []
        ordering = ['name', 'city']
        ordering_fields = ['city', 'name', {'work_time': ['begin_time']}, 'address', 'phone']

    def get_city(self, obj):
        return obj.city.name


class BranchDetailSerializer(serializers.ModelSerializer):
    managers = serializers.SerializerMethodField(label='Администраторы')
    staffs = serializers.SerializerMethodField(label='Специалисты')

    class Meta:
        model = Branch
        fields = [
            'id', 'city', 'name', 'phone', 'address', 'begin_time', 'end_time', 'managers', 'staffs'
        ]
        ordering = BranchSerializer.Meta.ordering
        ordering_fields = BranchSerializer.Meta.ordering_fields
        select_related = []
        prefetch_related = []
        widgets = dict(
            managers=dict(class_name='ManyRelatedField', model='company.Manager'),
            staffs=dict(class_name='ManyRelatedField', model='company.Staff')
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in ['begin_time', 'end_time']:
            self.fields[field].format = "%H:%M"

    def get_managers(self, obj):
        return [i.person_id for i in obj.crew.filter(role='manager')]

    def get_staffs(self, obj):
        return [i.person_id for i in obj.crew.filter(role='staff')]

    # def validate(self, data):
    #     data['city_id'] = data.pop('city_id', None)
    #     print('data', data)
    #     return data

    # def create(self, validated_data):
    #     print('validated_data', validated_data)
    #     return Branch(**validated_data)
