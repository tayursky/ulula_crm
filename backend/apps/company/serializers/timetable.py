from datetime import datetime, timedelta

from rest_framework import serializers

from apps.company.models import Branch, Timetable


class TimetableSerializer(serializers.ModelSerializer):
    cache = serializers.ReadOnlyField()

    class Meta:
        model = Timetable
        fields = [
            'id', 'branch', 'staff', 'begin', 'end', 'begin_really', 'end_really', 'comment', 'cache'
        ]
        headers = []
        form_fields = [
            'branch', 'staff', 'begin', 'end', 'begin_really', 'end_really', 'comment'
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance:
            self.instance = self.get_instance_with_timezone(self.instance)
            # self.fields['start_datetime'].initial = self.context.get('user')
        elif self.context.get('request') and self.context['request'].data.get('day'):
            data = self.context['request'].data
            branch = Branch.objects.get(pk=data.get('branch'))
            day = datetime.strptime(data.get('day'), '%Y-%m-%d')
            begin = day.replace(hour=branch.begin_time.hour, minute=branch.begin_time.minute)
            end = day.replace(hour=branch.end_time.hour, minute=branch.end_time.minute)
            self.fields['branch'].initial = branch.id
            self.fields['staff'].initial = data.get('staff')
            self.fields['begin'].initial = begin
            self.fields['end'].initial = end

    def validate(self, data):
        data = self.get_data_without_timezone(data)
        return data

    def create(self, validated_data):
        instance = Timetable.objects.create(**validated_data)
        return instance

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        return instance

    @staticmethod
    def get_instance_with_timezone(instance):
        if isinstance(instance, list) or isinstance(instance, type(Timetable.objects.none())):
            for item in instance:
                timezone = item.branch.city.timezone
                item.begin += timedelta(hours=timezone)
                item.end += timedelta(hours=timezone)
        else:
            timezone = instance.branch.city.timezone
            instance.begin += timedelta(hours=timezone)
            instance.end += timedelta(hours=timezone)
        return instance

    @staticmethod
    def get_data_without_timezone(data):
        timezone = data['branch'].city.timezone
        data['begin'] -= timedelta(hours=timezone)
        data['end'] -= timedelta(hours=timezone)
        return data
