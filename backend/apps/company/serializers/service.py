from rest_framework import serializers

from apps.company.models import Service, ServiceGroup, ServiceExecutor


class ServiceGroupSerializer(serializers.ModelSerializer):
    services_display = serializers.ReadOnlyField(source='get_services_display')

    class Meta:
        model = ServiceGroup
        fields = ['id', 'name', 'comment', 'services_display']
        headers = ['name', dict(value='services_display', label='Услуги'), 'comment']
        form_fields = ['name', 'comment']


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ['id', 'group', 'name', 'cost', 'cost_kid', 'time', 'comment']
        headers = [dict(value='group', related='group'), 'name', 'time', 'cost', 'cost_kid']
        form_fields = ['group', 'name', 'cost', 'cost_kid', 'time', 'comment']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in ['time']:
            self.fields[field].format = "%H:%M"


class ServiceExecutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServiceExecutor
        fields = ['id', 'service', 'staff', 'cost', 'cost_kid', 'time']
        headers = [
            dict(value='staff', related='staff'), dict(value='service', related='service'),
            'time', 'cost', 'cost_kid'
        ]
        form_fields = ['service', 'staff', 'cost', 'cost_kid', 'time']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in ['time']:
            self.fields[field].format = "%H:%M"
