# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from apps.directory.models import WorkingCalendar


class Command(BaseCommand):
    help = 'calendar parse'

    def handle(self, *args, **options):
        print('\nStart calendar_parse\n')

        WorkingCalendar.calendar_parse(2019)

        print('\nFinish')
