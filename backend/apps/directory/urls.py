from django.urls import path, include
from rest_framework import routers

from .viewsets import CityViewSet, WorkingCalendarViewSet

router = routers.DefaultRouter()
urlpatterns = [
    # path('city/get_form/', CityViewSet.as_view({'get': 'get_form'})),
    # path('city/', CityViewSet.as_view({'get': 'list'})),
    path('', include(router.urls)),
]
router.register('city', CityViewSet, basename='city')
router.register('working_calendar', WorkingCalendarViewSet, basename='working_calendar')
urlpatterns += router.urls
