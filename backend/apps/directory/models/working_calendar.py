import calendar
import requests
from datetime import datetime, date
from bs4 import BeautifulSoup

from django.db import models
from django.db.models import Q
from django.utils.translation import gettext as _

from common.models import BaseModel


class WorkingCalendar(BaseModel):
    """
        Производственный календарь
    """
    DAY_TYPE = (
        ('workday', 'Рабочий день'),
        ('weekend', 'Выходной'),
        ('pre_holiday', 'Предпраздничный день'),
        ('holiday', 'Праздник')
    )
    day = models.DateField(_('Day'), primary_key=True)
    day_type = models.CharField(_('Day type'), max_length=32, choices=DAY_TYPE, default='workday')
    description = models.TextField(_('Description'), default='')

    class Meta:
        verbose_name = 'Производственный календарь'
        verbose_name_plural = 'Производственный календарь'
        ordering = ['day']
        default_permissions = ()
        permissions = [
            ('add_workingcalendar', 'Добавлять производственный календарь'),
            ('view_workingcalendar', 'Просматривать производственный календарь')
        ]
        icon = 'el-icon-date'

    def __str__(self):
        description = ' (%s)' % self.description if self.description else ''
        return '%s: %s %s' % (self.day, self.day_type, description)

    @staticmethod
    def get_timesheet(**kwargs):
        year = int(kwargs.get('year') or date.today().year)
        month, day = kwargs.get('month'), kwargs.get('day')
        begin, end = kwargs.get('begin'), kwargs.get('end')
        months = dict()

        if begin and end:
            year = begin.year
            _q = Q(day__gte=begin, day__lte=end)
        else:
            _q = Q(day__year=year)
            if day:
                if isinstance(day, date):
                    _day = day
                else:
                    try:
                        _day = datetime.strptime(day, '%Y-%m-%d').date()
                    except ValueError:
                        _day = date(int(year), int(month), int(day))
                    except TypeError:
                        _day = date.today()
                _q = Q(day=_day)
            elif month:
                _q &= Q(day__month=month)

        qs = WorkingCalendar.objects.filter(_q)
        for day in qs.values('day', 'day_type'):
            if day['day'].month not in months.keys():
                months[day['day'].month] = dict(
                    number=day['day'].month,
                    label=_(day['day'].strftime('%B')),
                    days=[],
                    weeks=[]
                )
            day_week = day['day'].isocalendar()[1]
            if day_week not in months[day['day'].month]['weeks']:
                months[day['day'].month]['weeks'].append(day_week)

            months[day['day'].month]['days'].append(dict(
                date=day['day'].strftime('%Y-%m-%d'),
                day=day['day'].day,
                day_type=day['day_type'],
                weekday=day['day'].weekday(),
                week=day_week
            ))

        months_collect = []
        for key, month in months.items():
            workdays = len(list(filter(lambda x: x['day_type'] == 'workday', month['days'])))
            weekends = len(list(filter(lambda x: x['day_type'] == 'weekend', month['days'])))
            pre_holidays = len(list(filter(lambda x: x['day_type'] == 'pre_holiday', month['days'])))
            holidays = len(list(filter(lambda x: x['day_type'] == 'holiday', month['days'])))
            month.update(
                annotate=dict(
                    hours=workdays * 8 + pre_holidays * 7,
                    workdays=workdays,
                    weekends=weekends,
                    pre_holidays=pre_holidays,
                    holidays=holidays
                )
            )
            months_collect.append(month)

        annotate = dict()
        for month in months_collect:
            if not annotate:
                annotate = month['annotate'].copy()
            else:
                for key, value in month['annotate'].items():
                    annotate[key] += value

        answer = dict(
            annotate=annotate,
            months=months_collect,
            weekdays=[_(i.strip()) for i in calendar.weekheader(3).split(' ')],
            year=year
        )

        if kwargs.get('only_days'):
            answer['days'] = []
            for _month in months_collect:
                answer['days'] += _month['days']
            answer.pop('months')
            answer.pop('year')

        return answer

    @staticmethod
    def parse(year):
        year = int(year)
        url = 'https://its.1c.ru/calendar/work%s' % year
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:45.0) Gecko/20100101 Firefox/45.0'
        }
        r = requests.get(url, headers=headers)
        _html = BeautifulSoup(r.content, 'html.parser')
        _month = 0
        _begin_day = date(year, 1, 1)
        _end_day = date(year, 12, 31)
        _days = list()
        for el_months in _html.select('div.calendar_wrk div.month_inner'):
            _month += 1
            for el_day in el_months.select('li.calendar_date'):
                _day = None
                try:
                    _day = int(el_day.get_text())
                except ValueError:
                    continue
                el_class = el_day.attrs.get('class')
                _type = 'workday'
                if 'holiday' in el_class:
                    _type = 'holiday'
                elif 'short' in el_class:
                    _type = 'pre_holiday'
                elif 'weekend' in el_class:
                    _type = 'weekend'
                _days.append(dict(
                    day=date(year, _month, _day),
                    day_type=_type,
                    description=el_day.attrs.get('title') or ''
                ))
        WorkingCalendar.objects.filter(day__range=(_begin_day, _end_day)).delete()
        WorkingCalendar.objects.bulk_create([WorkingCalendar(**data) for data in _days])
        return True

    @staticmethod
    def parse_old(year):
        year = int(year)
        url = 'https://www.buhonline.ru/workcalendar/%s' % year
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'
        }
        r = requests.get(url, headers=headers)
        _html = BeautifulSoup(r.content, 'html.parser')
        _month = 0
        _begin_day = date(year, 1, 1)
        _end_day = date(year, 12, 31)
        _days = list()
        for el_months in _html.select('table.calendar table.calendar-month'):
            print(el_months)
            _month += 1
            for el_day in el_months.select('td.day-mark'):
                _day = None
                try:
                    _day = int(el_day.get_text())
                except ValueError:
                    continue
                el_class = el_day.span.attrs.get('class')
                _type = 'workday'
                if 'day-red' in el_class:
                    _type = 'weekend'
                elif 'day-blue' in el_class:
                    _type = 'holiday'
                elif 'day-grey' in el_class:
                    _type = 'pre_holiday'
                _days.append(dict(
                    day=date(year, _month, _day),
                    day_type=_type,
                    description=el_day.attrs.get('title') or ''
                ))
        WorkingCalendar.objects.filter(day__range=(_begin_day, _end_day)).delete()
        WorkingCalendar.objects.bulk_create([WorkingCalendar(**data) for data in _days])
        return True
