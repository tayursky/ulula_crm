from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from common.models import BaseModel


class City(BaseModel):
    name = models.CharField('Наименование', max_length=124, unique=True)
    timezone = models.IntegerField('Часовой пояс', default=0,
                                   validators=[MaxValueValidator(14), MinValueValidator(-12)])

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'
        ordering = ('name',)
        default_permissions = ()
        permissions = [
            ('add_city', 'Добавлять города'),
            ('change_city', 'Редактировать города'),
            ('delete_city', 'Удалять города'),
            ('view_city', 'Просматривать города')
        ]
        icon = 'el-icon-office-building'
        search_fields = ['name']
        filters = dict(
            ordering=['name', 'timezone'],
            fields=dict(
                name=dict(
                    class_name='CharField', label='Наименование', key='name__icontains',
                    widget=dict(attrs={}, name="TextInput", input_type="text")
                ),
                timezone=dict(
                    class_name='IntegerField', label='Часовой пояс', key='timezone',
                    widget=dict(attrs={}, name="TextInput", input_type="number")
                )
            )
        )
        headers = ['name', 'timezone']
        form_fields = ['name', 'timezone']

    def __str__(self):
        return self.name
