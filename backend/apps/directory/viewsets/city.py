from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.directory.models import City
from apps.directory.serializers import CitySerializer


class CityViewSet(ModelViewSet):
    model = City
    serializer_class = CitySerializer

    def get_queryset(self):
        return self.model.objects.all()

    def list(self, request, *args, **kwargs):
        print(request.data)
        # serializer = self.serializer_class(self.get_queryset(), many=True)
        # result = dict(
        #     results=serializer.data,
        #     test=12
        # )
        # import ipdb; ipdb.set_trace()
        result = super().list(self, request, *args, **kwargs)
        # result.data.update(dict(
        #     get_form=self.get_serializer().get_form()
        # ))
        return result

    def retrieve(self, request, *args, **kwargs):
        result = super().retrieve(self, request, *args, **kwargs)
        # result.data.update(dict(
        #     get_form=self.get_serializer().get_form()
        # ))
        return result

    @action(detail=False, methods=['get'])
    def get_form(self, request, *args, **kwargs):
        return Response(self.get_serializer().form_dict())

    @action(detail=True, methods=['get'])
    def form(self, request, *args, **kwargs):
        # print(request.data)
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        # result = super().retrieve(self, request, *args, **kwargs)
        # result.data.update(dict(
        #     json=True
        # ))
        return Response(serializer.get_form())
