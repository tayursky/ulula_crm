from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.directory.models import WorkingCalendar
from apps.directory.serializers import WorkingCalendarSerializer


class WorkingCalendarViewSet(ModelViewSet):
    """
        Производственный календарь
    """
    model = WorkingCalendar
    serializer_class = WorkingCalendarSerializer

    def get_queryset(self):
        return self.model.objects.all()

    def list(self, request, *args, **kwargs):
        year = request.GET.get('year')
        month = request.GET.get('month')
        day = request.GET.get('day')
        return Response(dict(
            calendar=self.model.get_timesheet(year=year, month=month, day=day)
        ))

    @action(methods=['GET'], detail=False)
    def parse(self, request, *args, **kwargs):
        self.model.parse(request.GET.get('year'))
        return Response(dict(
            calendar=self.model.get_timesheet(year=request.GET.get('year'))
        ))
