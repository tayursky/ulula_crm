from rest_framework import serializers

from apps.directory.models import WorkingCalendar


class WorkingCalendarSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkingCalendar
        fields = '__all__'
