from rest_framework import serializers

from apps.directory.models import City


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ['id', 'name', 'timezone']
        ordering = ['name']
        ordering_fields = ['name', 'timezone']
