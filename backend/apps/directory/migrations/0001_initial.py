# Generated by Django 4.1.7 on 2023-04-13 09:50

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=124, unique=True, verbose_name='Наименование')),
                ('timezone', models.IntegerField(default=0, validators=[django.core.validators.MaxValueValidator(14), django.core.validators.MinValueValidator(-12)], verbose_name='Часовой пояс')),
            ],
            options={
                'verbose_name': 'Город',
                'verbose_name_plural': 'Города',
                'ordering': ['name'],
                'permissions': [('add_city', 'Добавлять города'), ('change_city', 'Редактировать города'), ('delete_city', 'Удалять города'), ('view_city', 'Просматривать города')],
                'default_permissions': (),
            },
        ),
        migrations.CreateModel(
            name='WorkingCalendar',
            fields=[
                ('day', models.DateField(primary_key=True, serialize=False, verbose_name='Day')),
                ('day_type', models.CharField(choices=[('workday', 'Рабочий день'), ('weekend', 'Выходной'), ('pre_holiday', 'Предпраздничный день'), ('holiday', 'Праздник')], default='workday', max_length=32, verbose_name='Day type')),
                ('description', models.TextField(default='', verbose_name='Description')),
            ],
            options={
                'verbose_name': 'Производственный календарь',
                'verbose_name_plural': 'Производственный календарь',
                'ordering': ['day'],
                'permissions': [('add_workingcalendar', 'Добавлять производственный календарь'), ('view_workingcalendar', 'Просматривать производственный календарь')],
                'default_permissions': (),
            },
        ),
    ]
