# Generated by Django 4.1.7 on 2023-04-02 09:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pubmed', '0012_remove_proxy_anonymous'),
    ]

    operations = [
        migrations.AddField(
            model_name='proxy',
            name='source',
            field=models.CharField(default='', max_length=256),
            preserve_default=False,
        ),
    ]
