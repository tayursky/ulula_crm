# Generated by Django 4.1.7 on 2023-03-26 14:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pubmed', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='title',
            field=models.TextField(null=True),
        ),
    ]
