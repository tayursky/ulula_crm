from decimal import Decimal

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404, redirect, reverse

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from identity.models import Person
from identity.serializers import PersonSerializer
# from utils.normalize_data import normalise_phone


class PersonViewSet(ModelViewSet):
    serializer_class = PersonSerializer
    model = Person
    queryset = Person.objects.all()
    full_name, last_name, first_name, patronymic = None, None, None, None
    phone = None

    @action(detail=False, methods=['GET'])
    def current(self, request):
        person = request.user.person
        data = self.get_serializer(person).data
        return Response(data, status=status.HTTP_200_OK)

    # def dispatch(self, request, *args, **kwargs):
    #     self.full_name = request.GET.get('full_name', '').strip().split(' ')
    #     self.last_name = self.full_name[0] if len(self.full_name) > 0 else None
    #     self.first_name = self.full_name[1] if len(self.full_name) > 1 else None
    #     self.patronymic = self.full_name[2] if len(self.full_name) > 2 else None
    #     self.phone = normalise_phone(request.GET.get('phone'))
    #     return super().dispatch(request, *args, **kwargs)
    #
    # def get_context_data(self, **kwargs):
    #     _qs = Person.objects.all()
    #     if self.last_name:
    #         _qs = _qs.filter(last_name__istartswith=self.last_name)
    #     if self.first_name:
    #         _qs = _qs.filter(first_name__istartswith=self.first_name)
    #     if self.patronymic:
    #         _qs = _qs.filter(patronymic__istartswith=self.patronymic)
    #     if self.phone:
    #         _qs = _qs.filter(phones__value__contains=self.phone)
    #     items = []
    #     for person in _qs[:7]:
    #         items.append(dict(
    #             id=person.id,
    #             birthday=person.birthday.strftime('%d.%m.%Y') if person.birthday else None,
    #             caсhe=person.cache,
    #         ))
    #     context = dict(
    #         items=items
    #     )
    #     return context
    #
    # def get(self, request, *args, **kwargs):
    #     context = self.get_context_data()
    #
    #     if self.request.GET.get('debug'):
    #         return render_to_response('debug.jinja2', locals())
    #     return JsonResponse(context)
