from decimal import Decimal
from django.core import serializers
from django.db.models import Avg, Sum
from django.urls import reverse, reverse_lazy
from django.http import HttpResponse, JsonResponse
from django.views.generic import DetailView, ListView, TemplateView, View
from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.forms.models import modelform_factory
from django.contrib.auth.views import LoginView

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.response import Response


class AuthTokenViewSet(ObtainAuthToken, ModelViewSet):

    @action(detail=False, methods=['POST'])
    def login(self, request):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        # Token.objects.filter(user=user).delete()
        # token = Token.objects.create(user=user)
        return Response(dict(token=token.key), status=status.HTTP_200_OK)

    @action(detail=False, methods=['POST'])
    def logout(self, request):
        try:
            Token.objects.get(user=request.user).delete()
        except TypeError:
            pass
        return Response(dict(logout='ok'), status=status.HTTP_200_OK)
