from datetime import date, datetime, timedelta

from rest_framework import serializers
from rest_framework.reverse import reverse_lazy

from common.serializers import CommonModelSerializer
from identity.models import Person, Contact


class ContactSerializer(CommonModelSerializer):
    class Meta:
        model = Contact
        fields = ['id', 'type', 'value', 'comment']
        form_fields = ['type', 'value', 'comment']


class PersonSerializer(CommonModelSerializer):
    # user = serializers.CharField(source='user')
    full_name = serializers.CharField(read_only=True, min_length=3, max_length=256, label='Ф.И.О.')
    contacts = serializers.SerializerMethodField()

    class Meta:
        model = Person
        fields = [
            'id', 'first_name', 'patronymic', 'last_name', 'full_name', 'birthday', 'timezone', 'comment',
            'address', 'passport_number', 'passport_issued', 'cache', 'user', 'contacts'
        ]
        list_headers = ['full_name', 'birthday', 'timezone']
        list_headers_label = dict(
            full_name=dict(label='Ф.И.О.', value='full_name')
        )
        list_form_fields = [
            'first_name', 'patronymic', 'last_name', 'full_name',
            'birthday', 'timezone', 'comment',
            'address', 'passport_number', 'passport_issued',
        ]
        form_fields = [
            'first_name', 'patronymic', 'last_name', 'full_name', 'birthday', 'comment',
            'address', 'passport_number', 'passport_issued'
        ]
        widgets = dict(
        )

    def __init__(self, *args, **kwargs):
        print('__init__')
        # print('full_name', self.initial_data.get('full_name'))
        # import ipdb; ipdb.set_trace()
        super().__init__(*args, **kwargs)

    def get_contacts(self, obj):
        return ContactSerializer(obj.contacts.all(), many=True).data

    def validate(self, data):
        # if not self.initial_data.get('begin'):
        #     raise serializers.ValidationError('Необходимо время начала')
        print('validate', data)
        return data

    def validate_first_name(self, value):
        print('validate_first_name', value)
        return value
