# Generated by Django 4.1.7 on 2023-04-13 20:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('directory', '0001_initial'),
        ('identity', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalperson',
            name='city',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='directory.city', verbose_name='Город'),
        ),
        migrations.AddField(
            model_name='historicalperson',
            name='sex',
            field=models.CharField(choices=[('', '--'), ('man', 'муж'), ('woman', 'жен')], default='', max_length=5, null=True, verbose_name='Пол'),
        ),
        migrations.AddField(
            model_name='person',
            name='city',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='directory.city', verbose_name='Город'),
        ),
        migrations.AddField(
            model_name='person',
            name='sex',
            field=models.CharField(choices=[('', '--'), ('man', 'муж'), ('woman', 'жен')], default='', max_length=5, null=True, verbose_name='Пол'),
        ),
    ]
