# Generated by Django 4.1.7 on 2023-08-26 16:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('identity', '0002_historicalperson_city_historicalperson_sex_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='comment',
            field=models.CharField(blank=True, max_length=512, null=True, verbose_name='Комментарий'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='type',
            field=models.CharField(choices=[('phone', 'телефон'), ('messendger', 'мессенджер'), ('social', 'соц.сеть'), ('other', 'другое')], max_length=128, verbose_name='Contact type'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='value',
            field=models.CharField(max_length=256, verbose_name='Contact'),
        ),
        migrations.AlterField(
            model_name='historicalcontact',
            name='comment',
            field=models.CharField(blank=True, max_length=512, null=True, verbose_name='Комментарий'),
        ),
        migrations.AlterField(
            model_name='historicalcontact',
            name='type',
            field=models.CharField(choices=[('phone', 'телефон'), ('messendger', 'мессенджер'), ('social', 'соц.сеть'), ('other', 'другое')], max_length=128, verbose_name='Contact type'),
        ),
        migrations.AlterField(
            model_name='historicalcontact',
            name='value',
            field=models.CharField(max_length=256, verbose_name='Contact'),
        ),
        migrations.AlterField(
            model_name='historicalperson',
            name='sex',
            field=models.CharField(blank=True, choices=[('', '--'), ('man', 'муж'), ('woman', 'жен')], default='', max_length=5, verbose_name='Пол'),
        ),
        migrations.AlterField(
            model_name='person',
            name='sex',
            field=models.CharField(blank=True, choices=[('', '--'), ('man', 'муж'), ('woman', 'жен')], default='', max_length=5, verbose_name='Пол'),
        ),
    ]
