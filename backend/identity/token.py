from rest_framework.authentication import TokenAuthentication


class PersonTokenAuthentication(TokenAuthentication):
    keyword = 'Bearer'
