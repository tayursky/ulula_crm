# from periods.period import TimeInterval as Period
from django.contrib.auth import views as auth_views
from django.urls import path, include

from rest_framework import routers

from .viewsets import AuthTokenViewSet, PersonViewSet

router = routers.DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
    # path('auth-token/', PersonAuthToken.as_view(), name='auth-token'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    # path('recover/', PersonRecoverView.as_view(), name='recover'),
]

router.register('auth', AuthTokenViewSet, basename='auth')
router.register('person', PersonViewSet, basename='person')
urlpatterns += router.urls
