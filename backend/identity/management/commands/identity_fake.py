# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from person.models import Person, PersonPhone, PersonEmail, PersonContact
from mlm.models import Agent
from utils.word import get_random_name, get_random_number


class Command(BaseCommand):
    help = 'fake person data'

    def handle(self, *args, **options):
        print('\nStart fake\n')

        for person in Person.objects.all():
            print(person)
            random_name = get_random_name(full_name=person.cache.get('full_name'))
            # person_attrs = ['last_name', 'first_name', 'patronymic']
            for index, item in enumerate(random_name.split(' ')):
                if index == 0:
                    person.last_name = item
                elif index == 1:
                    person.first_name = item
                elif index == 2:
                    person.patronymic = item
                # person_attr = getattr(person, person_attrs[index])
                # person_attr = item
            person.save()

        for mail in PersonEmail.objects.all():
            print(mail)
            random_name = get_random_number()
            mail.value = '%s@fake.com' % random_name
            mail.save()

        for phone in PersonPhone.objects.all():
            print(phone)
            phone.value = get_random_number()
            phone.save()

        for contact in PersonContact.objects.all():
            print(contact)
            random_name = get_random_number()
            contact.value = 'https://fake.com/%s' % random_name
            contact.save()

        for agent in Agent.objects.all():
            print(agent)
            agent.bank_account = ''
            agent.bank_account_fio = ''
            agent.social_link = ''
            agent.code = str(get_random_number())
            agent.save()

        print('\nFinish')
