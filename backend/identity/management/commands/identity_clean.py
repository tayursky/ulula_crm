# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from person.models import Person, PersonContact, PersonPhone


class Command(BaseCommand):
    help = 'Person contacts clean'

    def handle(self, *args, **options):
        print('\nStart cleaning\n')

        empty = ['', ' ', None]

        PersonContact.objects.filter(value__in=empty).delete()
        PersonPhone.objects.filter(value__in=empty).delete()

        print('\nDone')
