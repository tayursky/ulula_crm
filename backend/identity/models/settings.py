from datetime import date, datetime
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext as _

from common.models import BaseModel


class Settings(BaseModel):
    DEFAULT_UI = dict(
        side_bar=dict(
            permanent=True,
            mini_variant=False,
            expand_on_hover=False
        )
    )
    user = models.ForeignKey('identity.User', related_name='settings', verbose_name=_('Settings'),
                             on_delete=models.CASCADE)
    ui = models.JSONField(default=dict)

    class Meta:
        verbose_name = _('Settings')
        verbose_name_plural = _('Settings')
        default_permissions = ()

    def __str__(self):
        return self.user
