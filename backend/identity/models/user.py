from django.contrib.auth.models import AbstractUser, PermissionsMixin, UserManager, Group as AuthGroup

from safedelete.models import SafeDeleteModel
from safedelete.models import HARD_DELETE_NOCASCADE
from simple_history.models import HistoricalRecords

from common.models import BaseModel


class User(AbstractUser, SafeDeleteModel, BaseModel):
    first_name = None
    last_name = None
    date_joined = None
    last_login = None

    _safedelete_policy = HARD_DELETE_NOCASCADE
    history = HistoricalRecords()

    @property
    def full_name(self):
        if not self.person:
            return ''
        return ('%s %s %s' % (self.person.last_name, self.person.first_name, self.person.patronymic or '')).strip()