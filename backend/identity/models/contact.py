from datetime import date, datetime
# from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext as _

from safedelete.models import SafeDeleteModel
from safedelete.models import HARD_DELETE_NOCASCADE
from simple_history.models import HistoricalRecords
from common.models import BaseModel


class Contact(SafeDeleteModel, BaseModel):
    CONTACT_TYPE = (
        ('phone', 'телефон'),
        ('email', 'e-mail'),
        ('messenger', 'мессенджер'),
        ('social', 'соц.сеть'),
        ('other', 'другое'),
    )
    person = models.ForeignKey('identity.Person', related_name='contacts', verbose_name=_('Person'),
                               on_delete=models.CASCADE)
    type = models.CharField(max_length=128, choices=CONTACT_TYPE, verbose_name='Тип контакта')
    value = models.CharField(max_length=256, verbose_name='Контакт')
    comment = models.CharField(max_length=512, null=True, blank=True, verbose_name='Комментарий')

    _safedelete_policy = HARD_DELETE_NOCASCADE
    history = HistoricalRecords()

    class Meta:
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')
        ordering = ['type', 'value']
        default_permissions = ()

    def __str__(self):
        return self.value

    # def save(self, *args, **kwargs):
    #     self.value = self.value
    #     if self.value:
    #         if self.value[0] == '8':
    #             self.value = '7%s' % self.value[1:]
    #     super().save(*args, **kwargs)
    #     self.person.save()
