from datetime import date, datetime
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext as _

from safedelete.models import SafeDeleteModel
from safedelete.models import HARD_DELETE_NOCASCADE
from simple_history.models import HistoricalRecords
from common.models import BaseModel


class Person(SafeDeleteModel, BaseModel):
    """
        Персональные данные
    """
    SEX = (
        ('', '--'), ('man', 'муж'), ('woman', 'жен'),
    )
    first_name = models.CharField(max_length=128, verbose_name='Имя')
    patronymic = models.CharField(max_length=128, blank=True, verbose_name='Отчество')
    last_name = models.CharField(max_length=128, blank=True, verbose_name='Фамилия')
    sex = models.CharField(choices=SEX, max_length=5, blank=True, default='', verbose_name='Пол')
    birthday = models.DateField(null=True, verbose_name='День рождения')
    comment = models.TextField(blank=True, verbose_name=_('Comment'))
    token = models.CharField(max_length=32, null=True, unique=True, verbose_name=_('Token'))
    # mighty_call_user = models.ForeignKey('sip.MightyCallUser', blank=True, null=True, verbose_name='Яндекс телефония',
    #                                      on_delete=models.CASCADE, help_text='Пользователь яндекс телефонии')
    city = models.ForeignKey('directory.City', null=True, on_delete=models.PROTECT, verbose_name='Город')
    timezone = models.SmallIntegerField(null=True, verbose_name='Часовой пояс')
    address = models.CharField(max_length=128, null=True, blank=True, verbose_name='Адрес регистрации')
    passport_number = models.CharField(max_length=32, null=True, blank=True, verbose_name='Паспорт серия и номер')
    passport_issued = models.CharField(max_length=128, null=True, blank=True, verbose_name='Выдавший орган')

    user = models.OneToOneField('identity.User', null=True, related_name='person', verbose_name=_('Account'),
                                on_delete=models.CASCADE)

    cache = models.JSONField(default=dict)

    _safedelete_policy = HARD_DELETE_NOCASCADE
    history = HistoricalRecords()

    filters_ordered = ['full_name']
    filters_fields = dict(
        full_name=dict(
            label='Ф.И.О.', key='cache__full_name__icontains',
            widget=dict(attrs={}, name='TextInput', input_type='text')
        )
    )

    class Meta:
        verbose_name = 'Персональные данные'
        verbose_name_plural = 'Персональные данные'
        # unique_together = ('first_name', 'patronymic', 'last_name', 'birthday')
        ordering = ('last_name', 'first_name', 'patronymic')
        default_permissions = ()
        permissions = (
            ('add_person', 'Добавлять персональные данные'),
            ('change_person', 'Изменять персональные данные'),
            ('delete_person', 'Удалять персональные данные'),
            ('view_person', 'Просматривать персональные данные'),
        )
        search_fields = ['first_name', 'patronymic', 'last_name']

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        return ' '.join([i for i in [self.last_name, self.first_name, self.patronymic] if i])

    @property
    def short_name(self):
        return ('%s %s' % (self.last_name.strip(), self.initials)).strip()

    @property
    def initials(self):
        answer = '%s.' % self.first_name.strip()[0] if self.first_name else ''
        answer += '%s.' % self.patronymic.strip()[0] if self.patronymic else ''
        return answer

    def cache_update(self):
        self.cache.update(dict(
            full_name=self.full_name,
            age=self.age,
            phones=self.phones
        ))
        return True

    @property
    def age(self):
        try:
            today = date.today()
            return today.year - self.birthday.year - (
                    (today.month, today.day) < (self.birthday.month, self.birthday.day))
        except AttributeError:
            return ''

    def get_age_display(self):
        return self.age

    @property
    def phones(self):
        try:
            return list(self.contacts.filter(type='phone').values_list('value', flat=True))
        except (AttributeError, ValueError):
            return list()

    def get_birthday_display(self):
        return self.birthday.strftime('%d.%m.%Y')

    def get_email(self):
        try:
            return self.emails.first().value
        except AttributeError:
            return ''

    def clean(self):
        if Person.objects.filter(
                cache__full_name='%s %s %s' % (self.last_name, self.first_name, self.patronymic),
                birthday=self.birthday) \
                .exclude(pk=self.id).exists():
            raise ValidationError({'last_name': u'Профайл с такими Ф.И.О. и датой рождения уже есть'})
        return super().clean()

    def save(self, *args, **kwargs):
        self.cache_update()
        super().save(*args, **kwargs)


class PersonSocialRelations(models.Model):
    """
        Социальные связи
    """
    RELATIVE_TYPE = (
        (0, 'отец'),
        (1, 'мать'),
    )
    person = models.ForeignKey(Person, related_name='family', verbose_name='Родственные связи',
                               on_delete=models.CASCADE)
    relative_type = models.PositiveSmallIntegerField(choices=RELATIVE_TYPE, default=0, verbose_name='Отношения')
    relative = models.ForeignKey(Person, related_name='relative', verbose_name='Родственник',
                                 on_delete=models.CASCADE)

    parent_rel = 'person'
    list_detail_fields = ['person', 'relative_type', 'relative']
    list_display = ['person', 'relative_type', 'relative']
    list_filters = ['person', 'relative_type', 'relative']
    list_form_fields = ['person', 'relative_type', 'relative']
    list_attrs = dict(
        person=dict(hidden=True),
        relative_type=dict(el_col=12),
        relative=dict(el_col=12, remote_search='directory/remote_search/identity/'),
    )
    child_list = ['detail']

    class Meta:
        verbose_name = 'Семейные связи'
        verbose_name_plural = 'Семейные связи'
        default_permissions = ()

    def __str__(self):
        return '%s - %s - %s' % (self.person, self.relative_type, self.relative)
