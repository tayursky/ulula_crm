from .token import PersonTokenAuthentication

__all__ = [
    'PersonTokenAuthentication'
]
