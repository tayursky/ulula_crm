import threading
import time

proxies = [f'proxy_{str(i).zfill(2)}' for i in range(1, 11)]
urls = [f'url_{str(i).zfill(3)}' for i in range(1, 101)]
data = {i: [] for i in proxies}
for index, url in enumerate(urls):
    data[proxies[index % 10]].append(url)

# print(data)

lock = threading.Lock()


def parse(proxy, urls):
    # global index
    # print(len(ids))
    # while index + 1 < len(ids):  # True:
    with lock:
        print(f'[{threading.current_thread().name}] - {proxy}, {urls}')
        # print(f'{ids[index]}')
        # index += 1
        time.sleep(.1)
    # locker.release()


thread_list = []
for proxy, urls in data.items():
    # print('i', i)
    thr = threading.Thread(target=parse, args=(proxy, urls)) #, name=f'thr-{item[0][-2:]}'
    thread_list.append(thr)
    thr.start()

for thr in thread_list:
    thr.join()
