import uuid
import warnings
from abc import ABC

from django.db import models
import django.db.models.options as options

options.DEFAULT_NAMES = options.DEFAULT_NAMES + ('search_fields', 'widgets')


class BaseModel(models.Model):
    """
        Базовый класс с атрибутами в Meta:
        search_fields: список полей по которым идет поиск
        widgets: виджеты
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Время создания")
    updated = models.DateTimeField(auto_now=True, blank=True, verbose_name="Время последнего изменения")

    class Meta:
        abstract = True


class CommonBaseModel(models.Model):
    """
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_column='uid')

    class Meta:
        abstract = True

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        # TODO здесь будет проверка на создание уведомления
        super().save(force_insert, force_update, using, update_fields)


class UUIDForeignKey(models.ForeignKey, ABC):

    def get_attname(self):
        return '%s_uid' % self.name


class SysNameNaturalKeyManager(models.Manager):
    use_in_migrations = True

    def get_by_natural_key(self, sys_name):
        return self.get(sys_name=sys_name)


class SysNameNaturalKeyMixin(models.Model):
    """
    Класс mixin добавляем поле sys_name, для доступа к объектам по системноему имени
    :param sys_name: Системное наименование
    :type sys_name: str
    """
    class Meta:
        abstract = True

    sys_name = models.CharField('Системеное наименование',
                                max_length=254,
                                unique=True,
                                db_index=True)

    objects = SysNameNaturalKeyManager()

    def natural_key(self):
        return [self.sys_name]