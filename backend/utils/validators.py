def validate_phone(data, length=11):
    data = data.replace(' ', '')
    if not data:
        return data
    if len(data) > length:
        raise ValueError('Максимальная длина: %s' % length)
    if not data.isnumeric():
        raise ValueError('Допускаются только цифры')

    return data
