import json
from django.utils.deprecation import MiddlewareMixin


class PutParsingMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if request.method == 'PUT' and request.content_type != 'application/json':
            if hasattr(request, '_post'):
                del request._post
                del request._files
            try:
                request.method = 'POST'
                request._load_post_and_files()
                request.method = 'PUT'
            except AttributeError as e:
                request.META['REQUEST_METHOD'] = 'POST'
                request._load_post_and_files()
                request.META['REQUEST_METHOD'] = 'PUT'
            request.PUT = request.POST


class CookData(MiddlewareMixin):

    @staticmethod
    def process_request(request):
        # print('CookData', request.method, request.content_type)

        if request.method in ['GET', 'DELETE']:
            request.params = request.GET.dict()

        elif request.method in ['POST', 'PUT']:
            try:
                if request.content_type == 'application/json':
                    request.data = json.loads(request.body.decode())
                else:
                    request.data = request.POST if request.method == 'POST' else request.PUT
                if 'multipart/form-data' in request.META.get('CONTENT_TYPE', ''):
                    request.data = normalize_data(request.data)

                for key, value in request.data.items():
                    if value in ['null', 'undefined']:
                        request.data[key] = None
                request.params = request.data.pop('params', '{}')
                try:
                    request.params = json.loads(request.params)
                except (TypeError, json.decoder.JSONDecodeError):
                    pass
                request.params = request.params[0] if isinstance(request.params, list) else request.params
            except AttributeError:
                return


def normalize_data(data):
    for key, item in data.items():
        try:
            data[key] = json.loads(item)
        except (json.decoder.JSONDecodeError, TypeError):
            if item and isinstance(item, str):
                if ['"', '"'] == [item[0], item[-1]]:
                    data[key] = item[1:-1]

    return data
