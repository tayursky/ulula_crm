import uuid
from django.contrib.contenttypes.models import ContentType
from django.db.models.fields import NOT_PROVIDED
from django.core.exceptions import FieldDoesNotExist
from rest_framework.fields import empty


# from rest_framework.reverse import reverse_lazy


class RemoteField:
    initial = None
    remote_search = '/common/search/'

    def __init__(self, name, field, **kwargs):
        self.name, self.field = name, field
        self.model, self.instance, self.widget = kwargs.get('model'), kwargs.get('instance'), kwargs.get('widget', {})

    def as_dict(self):
        try:
            default = self.field.default.id
        except (AttributeError, KeyError):
            default = self.field.default if self.field.default != empty else None
        if not default:
            try:
                default = self.model._meta.get_field(self.name).default
                default = default if default != NOT_PROVIDED else None
            except FieldDoesNotExist:
                pass
        if self.instance and hasattr(self.instance, self.name):
            self.initial = getattr(self.instance, self.name)
        elif self.field.initial:
            self.initial = self.field.initial
        self.initial = str(getattr(self.initial, 'id', self.initial))
        self.initial = None if self.initial in ['None'] else self.initial

        if not self.initial and default:
            self.initial = default

        return dict(
            name=self.name,
            label=self.widget.get('label', self.field.label),
            class_name=self.widget.get('class_name') or self.field.__class__.__name__,
            required=self.field.required,
            help_text=self.field.help_text,
            default=default,
            initial=self.widget.get('initial') or self.initial,
            widget=self.widget,
            error_messages=self.field.error_messages
        )


class RemotePrimaryKeyRelatedField(RemoteField):
    def as_dict(self):
        field_dict = super().as_dict()
        related_model = self.model._meta.get_field(self.name).related_model
        related_app = ContentType.objects.get_for_model(related_model)
        if self.initial in ['None']:
            self.initial = None
        initial = related_model.objects.filter(pk=self.initial)
        qs = []
        if len(initial):
            [initial] = initial
        if self.widget.get('choices_all'):
            qs = related_model.objects.all()
        elif not self.initial:
            qs = related_model.objects.all()
        choices = []
        if initial not in qs and initial:
            choices.append({'value': initial.id, 'title': str(initial)})
        for i in qs:
            item = {'value': i.id, 'title': str(i)}
            for extra_field in self.widget.get('choices_extra_fields', []):
                item[extra_field] = getattr(i, extra_field)
            choices.append(item)
        field_dict['choices'] = choices

        field_dict['widget'].update(dict(
            class_name='PrimaryKeyRelatedField',
            search_url=self.remote_search,  # reverse_lazy('remote_search')
            model=field_dict['widget'].get('model') or ('%s.%s') % (related_app.app_label, related_model.__name__),
        ))

        return field_dict


class RemoteManyRelatedField(RemoteField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict['default'] = []
        related_model = self.model._meta.get_field(self.name).related_model
        related_model_app = ContentType.objects.get_for_model(related_model)
        qs = related_model.objects.none()
        if self.widget.get('choices') == '__all__':
            qs = related_model.objects.all()
        elif self.instance:
            qs = getattr(self.instance, self.name).all()
        # import ipdb; ipdb.set_trace()
        choices = []
        for i in qs:
            item = {'value': i.id, 'title': str(i)}
            for extra_field in self.widget.get('choices_extra_fields', []):
                item[extra_field] = getattr(i, extra_field)
            choices.append(item)
        field_dict['choices'] = choices
        field_dict['widget']['search_url'] = self.remote_search  # reverse_lazy('remote_search')
        return field_dict


class RemoteCharField(RemoteField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict.update(dict(
            max_length=getattr(self.field, 'max_length', None),
            min_length=getattr(self.field, 'min_length', None)
        ))
        if self.field.style:
            field_dict['widget']['name'] = field_dict['widget'].get('name', 'Textarea')
        return field_dict


class RemoteIntegerField(RemoteField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict.update(dict(
            max_value=self.field.max_value,
            min_value=self.field.min_value
        ))
        return field_dict


class RemoteFloatField(RemoteIntegerField):
    def as_dict(self):
        return super().as_dict()


class RemoteDecimalField(RemoteIntegerField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict.update({
            'max_digits': self.field.max_digits,
            'decimal_places': self.field.decimal_places
        })
        return field_dict


class RemoteTimeField(RemoteField):
    def as_dict(self):
        field_dict = super().as_dict()
        return field_dict


class RemoteDateField(RemoteTimeField):
    def as_dict(self):
        return super().as_dict()


class RemoteDateTimeField(RemoteTimeField):
    def as_dict(self):
        return super().as_dict()


class RemoteEmailField(RemoteCharField):
    def as_dict(self):
        return super().as_dict()


class RemoteFileField(RemoteField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict['max_length'] = self.field.max_length
        return field_dict


class RemoteImageField(RemoteFileField):
    def as_dict(self):
        return super().as_dict()


class RemoteURLField(RemoteCharField):
    def as_dict(self):
        return super().as_dict()


class RemoteBooleanField(RemoteField):
    def as_dict(self):
        return super().as_dict()


class RemoteChoiceField(RemoteField):
    def as_dict(self):
        field_dict = super().as_dict()
        choices = [{'value': k, 'title': i} for k, i in getattr(self.field, 'choices' or {}).items()]
        field_dict.update(dict(
            choices=choices
        ))
        return field_dict


class RemoteTypedChoiceField(RemoteChoiceField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict.update({
            'empty_value': self.field.empty_value
        })
        return field_dict


class RemoteMultipleChoiceField(RemoteChoiceField):
    def as_dict(self):
        return super().as_dict()


class RemoteTypedMultipleChoiceField(RemoteMultipleChoiceField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict.update({
            'coerce': self.field.coerce,
            'empty_value': self.field.empty_value
        })
        return field_dict


class RemoteMultiValueField(RemoteField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict['fields'] = self.field.fields
        return field_dict


class RemoteFilePathField(RemoteChoiceField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict.update({
            'path': self.field.path,
            'match': self.field.match,
            'recursive': self.field.recursive
        })
        return field_dict


class RemoteSplitDateTimeField(RemoteMultiValueField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict.update({
            'input_date_formats': self.field.input_date_formats,
            'input_time_formats': self.field.input_time_formats
        })
        return field_dict


class RemoteIPAddressField(RemoteCharField):
    def as_dict(self):
        return super().as_dict()


class RemoteSlugField(RemoteCharField):
    def as_dict(self):
        return super().as_dict()


class RemoteAttach(RemoteField):
    def as_dict(self):
        return dict(
            class_name=self.widget['name'],
            label=self.widget.get('label', ''),
            widget=self.widget
        )


class RemoteMainFile(RemoteField):
    def as_dict(self):
        field_dict = super().as_dict()
        # field_dict['max_length'] = self.field.max_length
        return field_dict


class RemoteJSONField(RemoteField):
    def as_dict(self):
        field_dict = super().as_dict()
        field_dict['initial'] = '{}'
        field_dict['default'] = '{}'
        return field_dict


class RemoteSplitter(RemoteField):
    def as_dict(self):
        return dict(
            class_name=self.widget['name'],
            label=self.widget.get('label', ''),
            widget=self.widget
        )
