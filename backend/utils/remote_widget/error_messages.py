def clear_messages(field):
    result = dict()
    for key, message in field.get('error_messages', {}).items():
        if key == 'required' and field.get(key) == False:
            continue
        if not field.get(key) is None:
            result[key] = message.format(**{key: field[key]})
    field['error_messages'] = result
    return field
