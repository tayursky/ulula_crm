from collections import OrderedDict

from .error_messages import clear_messages
from . import remote_field as remote_field_class


class RemoteViewSet:
    serializer_class = None
    instance = None
    fields = None
    model = None
    meta = None

    def meta_dict(self, *args, **kwargs):
        result = dict(
            serializer_name=self.serializer_class.__class__.__name__,
            headers=[{'name': k, 'label': i.label} for k, i in self.serializer_class().fields.items() if k != 'id']
        )
        if self.model:
            result.update(
                app_label=self.model._meta.app_label,
                model_name=self.model.__name__,
                verbose_name=self.model._meta.verbose_name,
                verbose_name_plural=self.model._meta.verbose_name_plural
            )
        return result

    def form_dict(self, *args, **kwargs):
        return self.serializer_class().form_dict()
