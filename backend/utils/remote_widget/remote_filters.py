class RemoteFilters:

    def widget_filters(self, request):
        filters = []
        for name, item in getattr(self.Meta, 'remote', {}).items():
            data = dict(
                class_name=item.get('class_name') or self.filters[name].__class__.__name__,
                name=name,
                label=item.get('label') or self.filters[name].label,
            )
            if item.get('qs'):
                qs = getattr(self, item.get('qs'))(request)
                data['choices'] = [dict(value=i.id, title=i.__str__()) for i in qs]
            else:
                data['widget'] = item.get('widget')
            filters.append(data)

        return filters
