from rest_framework import serializers

from apps.directory.models import City


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'
