from rest_framework import viewsets

from apps.directory.models import City
from api.v1.serializers import TestSerializer


class Test(viewsets.ModelViewSet):
    model = City
    serializer_class = TestSerializer
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        query = self.model.objects.all()
        return query