from django.urls import path, include

from rest_framework import routers
from .v1 import viewsets

router = routers.DefaultRouter()
# router.register(r'v1/test', viewsets.Test)

urlpatterns = [
    path('appoint/', include(('apps.appoint.urls', 'appoint'), namespace='appoint-urls')),
    path('company/', include(('apps.company.urls', 'company'), namespace='company-urls')),
    path('common/', include(('common.urls', 'common'), namespace='common-urls')),
    path('directory/', include(('apps.directory.urls', 'directory'), namespace='directory-urls')),
    path('identity/', include('identity.urls')),

    path('', include(router.urls)),
]
