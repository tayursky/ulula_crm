import random
import threading
import time
from threading import BoundedSemaphore

from pubmed.parser.proxy_master import ProxyMaster

proxy_master = ProxyMaster(5)
max_connections = 5
pool = BoundedSemaphore(value=max_connections)

start = time.time()
def parse(_url):
    with pool:
        proxy = proxy_master.get()
        wait = random.randint(1, 5)
        print(f'{threading.current_thread().name}: - {_url} ({wait})', proxy['string'])
        time.sleep(wait)
        proxy_master.skip(proxy)


thread_list = []
for _url in [f'url_{str(i).zfill(3)}' for i in range(20)]:
    thr = threading.Thread(target=parse, args=(_url,))
    thread_list.append(thr)
    thr.start()

for thr in thread_list:
    thr.join()

end = time.time()
print(end-start)

print(proxy_master.proxies)
print(proxy_master.stat())
