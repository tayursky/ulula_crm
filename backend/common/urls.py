from django.urls import include, path, re_path
from rest_framework import routers

from common.views import CommonModelViewSet, SearchViewSet

router = routers.SimpleRouter()
router.register('model/(?P<model_label>\w+)', CommonModelViewSet, basename='common-model-viewset')
router.register('search', SearchViewSet, basename='remote_search')

urlpatterns = [
    # path('model/<str:model>/', CommonModelViewSet.as_view({'get': 'list'}), name='common_model_viewset'),
    path('remote_search', SearchViewSet.as_view({'get': 'list'}), name='remote_search'),
    path('', include(router.urls)),
]
