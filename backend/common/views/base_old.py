import json

from django.apps import apps
from django.core.exceptions import FieldDoesNotExist
from rest_framework import viewsets, status
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

# from api.v1.directory.serializers import *  # noqa
# from apps.company import *  # noqa
from utils.choices import filters_choices


class BaseViewSet(viewsets.ModelViewSet):
    data = {}
    model = None
    ordering = []
    filters = {}
    form_fields = []
    form_fields_exclude = []

    def dispatch(self, request, *args, **kwargs):
        request.params['headers'] = json.loads(request.params.get('headers', '[]'))
        self.pagination_class.page_size = int(request.params.get('page_size') or self.pagination_class.page_size)
        self.form_fields = json.loads(request.params.get('fields', '[]'))
        self.form_fields_exclude = json.loads(request.params.get('fields_exclude', '[]'))

        if self.serializer_class:
            self.model = self.serializer_class.Meta.model
        else:
            try:
                self.serializer_class = eval(request.params.get('serializersName'))
                self.model = self.serializer_class.Meta.model
            except TypeError:
                self.model = self.model or self.get_model()
                self.serializer_class = self.get_serializer_class()

        filters = getattr(self, 'get_filters', self.model.get_filters)(request)
        self.filters = filters_choices(request, filters, self.model)
        return super().dispatch(request, *args, **kwargs)

    def get_model(self):
        model_name = self.request.params.get('model')
        return apps.get_model(model_name)

    def get_queryset(self):
        return self.model.objects.all()

    def get_serializer_class(self):
        serializer_class = self.serializer_class or eval('%sSerializer' % self.model.__name__)
        form_fields = self.form_fields or getattr(serializer_class.Meta, 'form_fields', [])
        form_fields = [i for i in form_fields if i not in self.form_fields_exclude]
        serializer_class.Meta.form_fields = form_fields
        return serializer_class

    def get_context(self):
        context = dict(
            request=self.request,
            user=self.request.user,
            form_fields=self.form_fields
        )
        return context

    def list(self, request, *args, **kwargs):
        self.ordering = self.get_ordering(request)
        qs = self.get_queryset() \
            .filter(self.model.get_filters_q(request, self.filters)) \
            .order_by(*self.ordering)
        try:
            page = self.paginate_queryset(qs)
        except NotFound:
            request.query_params._mutable = True
            request.query_params[self.pagination_class.page_query_param] = 1
            page = self.paginate_queryset(qs)
        items = self.serializer_class(page, many=True, context=self.get_context()).data
        headers, related = self.get_headers(items)
        data = dict(
            filters=self.filters,
            paginator=dict(
                page_size=self.pagination_class.page_size,
                count=qs.count(),
                page=int(request.query_params.get(self.pagination_class.page_query_param) or 1),
                ordering=self.ordering
            ),
            items=items,
            headers=headers,
            related=related,
            # form=RemoteWidget(self.serializer_class, context=self.get_context()).as_dict()
        )
        return Response(data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        context = dict(
            # form=RemoteWidget(self.serializer_class, instance=instance).as_dict(),
            data=self.get_serializer(instance).data
        )
        return Response(context)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        items = json.loads(request.data.get('items', '[]'))
        if items:
            self.model.objects.filter(pk__in=items).delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return super().destroy(request, *args, **kwargs)

    def get_headers(self, items):
        """
        :param items: данные от сериализатора
        :return: список с заголовками и данные __str__() из связанных таблиц
        """
        headers, related = [], dict()
        headers_source = self.request.params['headers'] or \
                         getattr(self.serializer_class.Meta, 'headers') or \
                         getattr(self.model._meta, 'headers', [])
        for head in headers_source:
            _head = dict()
            if isinstance(head, str):
                _head['value'] = head
            elif isinstance(head, dict):
                _head = head.copy()
            try:
                _head['text'] = _head.get('label') or self.model._meta.get_field(_head['value']).verbose_name
            except FieldDoesNotExist:
                _head['text'] = _head['value']
            headers.append(_head)

        serializer_fields = getattr(self.serializer_class.Meta, 'fields', [])
        if serializer_fields == '__all__':
            serializer_fields = [i.name for i in self.serializer_class.Meta.model._meta.fields]
        for head in serializer_fields:
            _model = self.model
            try:
                for _head in head.split('__'):
                    field = _model._meta.get_field(_head)
                    _model = field.related_model
                if field.related_model:
                    related_ids = []
                    for i in items:
                        if i[head]:
                            if isinstance(i[head], list):
                                related_ids = [str(i) for i in i[head]]
                            else:
                                related_ids.append(str(i[head]))
                    qs = field.related_model.objects.filter(pk__in=related_ids)
                    try:
                        related[head] = {str(i.id): i.__str__() for i in qs}
                    except ValueError as e:
                        print('Field:', head, e)
                elif field.choices:
                    related[head] = {i[0]: i[1] for i in field.choices}
            except FieldDoesNotExist:
                pass

        return headers, related

    def get_ordering(self, request):
        ordering = []
        for order in json.loads(request.params.get('ordering', '[]')) or self.model._meta.ordering:
            try:
                _order = order[1:] if order[0] == '-' else order
                self.model._meta.get_field(_order)  # Проверка есть ли такое поле в модели
            except FieldDoesNotExist:
                continue
            ordering.append(order)
        # print(ordering or self.model._meta.get('ordering', []))
        return ordering or self.model._meta.ordering
