from django.apps import apps
from django.db.models import Q

from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status

SEARCH_FIELDS = {
    'directory.city': ['name'],
    'company.branch': ['city__name', 'name'],
    'company.manager': ['last_name', 'first_name'],
    'company.staff': ['last_name', 'first_name']
}


class SearchViewSet(ModelViewSet):
    model_key = ''
    model = None
    query = ''
    ids = []
    search_fields = []

    def dispatch(self, request, *args, **kwargs):
        label = request.GET.get('model').lower()
        self.model = apps.get_model(*label.split('.'))
        self.search_fields = SEARCH_FIELDS.get(label)
        self.query = request.GET.get('q', '')
        self.ids = request.GET.get('ids')
        if self.ids:
            self.ids = self.ids.split(',') if isinstance(self.ids, str) else self.ids
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        q = Q()
        print('self.ids', self.ids)
        if self.ids:
            q = Q(pk__in=self.ids)
        else:
            for field in self.search_fields:
                q |= Q(**{('%s__icontains' % field): self.query})
        print('q', q)
        return self.model.objects.filter(q)[:30]

    def list(self, request, *args, **kwargs):
        return Response(dict(
            results=[dict(value=i.id, title=i.__str__()) for i in self.get_queryset()]
        ), status=status.HTTP_200_OK)
