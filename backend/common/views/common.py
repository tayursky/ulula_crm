from django.apps import apps
from django.db.models import Prefetch

from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework.response import Response

from utils.remote_widget import RemoteViewSet
from ..serializers import CommonModelSerializer
from apps.appoint.serializers import StageSerializer
from apps.company.serializers import BranchSerializer, BranchDetailSerializer
from apps.directory.serializers import CitySerializer

COMMON_MODELS = dict(
    appoint=dict(app='appoint', model='Appoint'),
    stage=dict(app='appoint', model='Stage', serializer=StageSerializer),
    branch=dict(app='company', model='Branch', serializer=BranchSerializer),
    staff=dict(app='company', model='Staff'),
    service=dict(app='company', model='Service'),
    service_group=dict(app='company', model='ServiceGroup'),
    service_executor=dict(app='company', model='ServiceExecutor'),
    city=dict(app='directory', model='City', serializer=CitySerializer)
)


class CommonModelViewSet(ModelViewSet, RemoteViewSet):
    label = ''
    item = {}
    ordering = []

    def dispatch(self, request, *args, **kwargs):
        self.label = self.label or kwargs.get('model_label')
        self.item = COMMON_MODELS.get(self.label)
        self.serializer_class = self.serializer_class or self.item.get('serializer', CommonModelSerializer)
        if not self.serializer_class.Meta.model:
            self.serializer_class.Meta.model = apps.get_model(app_label=self.item['app'], model_name=self.item['model'])
        self.model = self.serializer_class.Meta.model
        self.ordering = self.request.GET.getlist('ordering[]')
        if self.ordering is None:
            self.ordering = getattr(self.serializer_class.Meta, 'ordering', [])
        return super().dispatch(request, *args, **kwargs)

    def get_ordering(self, qs=False):
        data = []
        for item in self.ordering:
            for order in getattr(self.serializer_class.Meta, 'ordering_fields', []):
                if isinstance(order, str) and order == item:
                    data.append(item)
                elif isinstance(order, str) and item[:1] == '-' and order == item[1:]:
                    data.append(item)
                elif isinstance(order, dict) and item in order.keys():
                    data += order[item] if qs else [item]
                elif isinstance(order, dict) and item[:1] == '-' and item[1:] in order.keys():
                    data += ['-' + i for i in order[item[1:]]] if qs else [item]
        return data

    def get_ordering_fields(self):
        data = []
        for order in getattr(self.serializer_class.Meta, 'ordering_fields', []):
            if isinstance(order, str):
                data.append(order)
            elif isinstance(order, dict):
                data += order.keys()
        return data

    def get_queryset(self):
        select_related = getattr(self.serializer_class.Meta, 'select_related', [])
        prefetch_related = getattr(self.serializer_class.Meta, 'prefetch_related', [])
        # for name, field in self.serializer_class().fields.items():
        #     if field.__class__.__name__ == 'PrimaryKeyRelatedField':
        #         select_related.append(name)
        #     elif field.__class__.__name__ == 'ManyRelatedField':
        #         prefetch_related.append(name)
        # print('select_related', select_related)
        # print('prefetch_related', prefetch_related)
        qs = self.model.objects.all().select_related(*select_related).prefetch_related(*prefetch_related) \
            .order_by(*self.get_ordering(True))

        # if self.label == 'branch':
        #     from apps.company.models import BranchCrew
        #     qs = qs.prefetch_related(Prefetch('crew', queryset=BranchCrew.objects.select_related('person')))
        return qs

    def list(self, request, *args, **kwargs):
        # serializer = self.serializer_class(self.get_queryset(), many=True)
        # result = dict(
        #     results=serializer.data,
        # )
        result = super().list(self, request, *args, **kwargs)
        result.data.update(
            ordering=self.ordering
        )
        for item in ['next', 'previous']:
            result.data.pop(item, None)
        return result

    def retrieve(self, request, *args, **kwargs):
        result = super().retrieve(self, request, *args, **kwargs)
        # result.data.update(dict(
        #     get_form=self.get_serializer().get_form()
        # ))
        return result

    @action(detail=False, methods=['get'])
    def case(self, request, *args, **kwargs):
        self.serializer_class = self.item.get('serializer', CommonModelSerializer)
        result = self.meta_dict()
        result.update(
            form=self.form_dict(),
            ordering_fields=self.get_ordering_fields(),
            ordering=getattr(self.serializer_class.Meta, 'ordering', [])
        )
        return Response(result)

    @action(detail=True, methods=['get'])
    def form(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.form_dict())
