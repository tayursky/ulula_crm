import json
from datetime import datetime

from django.db import connection, models
from django.db.models import Q, options

options.DEFAULT_NAMES = options.DEFAULT_NAMES + (
    'select_related',
    'prefetch_related',
    'filters',  # фильтры
    'headers',  # заголовки в списке (таблице). [dict(value='city', text='Город', related='city'), 'name', 'address']
    'search_fields',  # список полей для поиска по-умолчанию
    'form_fields',  # поля для формы
    'icon',  # иконка
    'widgets'  # виджеты
)


class BaseModel(models.Model):
    """
        Базовый класс с доп.атрибутами в Meta
    """
    class Meta:
        abstract = True

    # @classmethod
    # def get_filters(cls, request, filters=None):
    #     """Возвращает набор фильтров"""
    #
    #     filters = filters or getattr(cls._meta, 'filters', {})
    #     if filters:
    #         filters['data'] = json.loads(request.GET.get('filters', '{}')) or \
    #                           {i: None for i in filters['fields'].keys()}
    #     return filters
    #
    # @classmethod
    # def get_filters_q(cls, request, filters=None, get_dict=None):
    #     """Возвращает фильтры в виде Q-объекта"""
    #
    #     filters = filters or cls.get_filters(request)
    #     q = Q()
    #     q_kwargs = dict()
    #     for key, field in filters.get('fields', {}).items():
    #         _kwargs = dict()
    #         if field['widget'].get('input_type') == 'daterange':
    #             try:
    #                 _range = json.loads(request.GET.get(key))
    #             except TypeError:
    #                 continue
    #             try:
    #                 dt_range_begin = datetime.strptime(_range[0], '%d.%m.%Y')
    #                 dt_range_finish = datetime.strptime(_range[1], '%d.%m.%Y').replace(hour=23, minute=59)
    #             except (IndexError, TypeError):
    #                 continue
    #             _kwargs = {
    #                 ('%s__gte' % key): dt_range_begin, ('%s__lte' % key): dt_range_finish
    #             }
    #             filters['data'][key] = [_range[0], _range[1]]
    #             continue
    #
    #         elif filters.get('data', {}).get(key):
    #             _kwargs = {field['key']: filters['data'][key]}
    #         if _kwargs and get_dict:
    #             q_kwargs.update(_kwargs)
    #         q &= Q(**_kwargs)
    #     return q_kwargs if get_dict else q

    @classmethod
    def truncate(cls):
        with connection.cursor() as cursor:
            cursor.execute(f'truncate table {cls._meta.db_table} restart identity cascade')
