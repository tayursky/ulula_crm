from .common import CommonModelSerializer

__all__ = [
    'CommonModelSerializer'
]
