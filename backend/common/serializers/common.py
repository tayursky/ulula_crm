from rest_framework.serializers import ModelSerializer

from utils.remote_widget.error_messages import clear_messages
from utils.remote_widget import remote_field as remote_field_class


class QuerySerializerMixin(object):
    related_fields = []  # for M2M fields
    prefetch_fields = []  # for ForeignKeys

    @classmethod
    def get_related_queries(cls, queryset):
        # This method we will use in our ViewSet
        # for modify queryset, based on RELATED_FIELDS and PREFETCH_FIELDS
        if cls.related_fields:
            queryset = queryset.select_related(*cls.related_fields)
        if cls.prefetch_fields:
            queryset = queryset.prefetch_related(*cls.prefetch_fields)
        return queryset


class CommonModelSerializer(QuerySerializerMixin, ModelSerializer):
    class Meta:
        model = None
        fields = '__all__'

    def form_dict(self, *args, **kwargs):
        meta = self.Meta
        form = []
        fields = getattr(meta, 'form_fields', []) or getattr(meta, 'fields')
        fields = self.get_fields().keys() if fields == '__all__' else fields
        for field_name in fields:
            field = self.get_fields().get(field_name)
            widget = getattr(meta, 'widgets', {}).get(field_name, {})
            field_dict = dict()
            try:
                field_class_name = widget.get('class_name', field.__class__.__name__)
                field_class = getattr(remote_field_class, f'Remote{field_class_name}')
                remote_field = field_class(field_name, field, model=meta.model, instance=self.instance, widget=widget)
                field_dict.update(**remote_field.as_dict())
            except Exception as e:
                print(f'Error serializing field {field_name}: {str(e)}')

            form.append(clear_messages(field_dict))
        return form
