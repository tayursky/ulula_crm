from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from rest_framework.authtoken import views as token_auth_views

urlpatterns = [
    path('api/', include('api.urls')),
    # path('auth-token/', token_auth_views.obtain_auth_token, name='auth-token'),
    path('admin/', admin.site.urls),
]
# urlpatterns += [path('silk/', include('silk.urls', namespace='silk'))]

if settings.DEBUG:
    urlpatterns += [
        path('silk/', include('silk.urls', namespace='silk')),
        # path('__debug__/', include('debug_toolbar.urls'))
    ]
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
