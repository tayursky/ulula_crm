from .branch import OldBranch
from .city import OldCity
from .deal import OldStage, OldDeal, OldDealPerson
from .person import OldAccount, OldPerson
from .service import OldServiceGroup, OldService, OldServiceMaster
from .timetable import OldTimeTable, OldTimeGroup

__all__ = [
    'OldBranch',
    'OldCity',
    'OldStage', 'OldDeal', 'OldDealPerson',
    'OldAccount', 'OldPerson',
    'OldServiceGroup', 'OldService', 'OldServiceMaster',
    'OldTimeTable', 'OldTimeGroup'
]
