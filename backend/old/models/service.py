from decimal import Decimal

from django.db import models


class OldServiceGroup(models.Model):
    """
        Группы услуг
    """
    name = models.CharField(max_length=124, verbose_name='Наименование')
    comment = models.TextField(null=True, blank=True, verbose_name='Комментарий')

    class Meta:
        managed = False
        db_table = 'deal_servicegroup'
        verbose_name = 'Группа услуг'
        verbose_name_plural = 'Группы услуг'
        ordering = ['name']
        default_permissions = ()

    def __str__(self):
        return self.name


class OldService(models.Model):
    """
        Услуга
    """
    group = models.ForeignKey(OldServiceGroup, null=True, related_name='services', verbose_name='Группа услуг',
                              on_delete=models.PROTECT)
    name = models.CharField(max_length=124, verbose_name='Наименование')
    cost = models.DecimalField(default='0.00', max_digits=30, decimal_places=2, verbose_name='Стоимость')
    cost_kid = models.DecimalField(default='0.00', max_digits=30, decimal_places=2, verbose_name='Стоимость (ребенок)')
    diagnostic = models.DecimalField(max_digits=30, decimal_places=2, verbose_name='Стоимость диагностики',
                                     default=Decimal('0.00'))
    time = models.TimeField(null=True, blank=True, verbose_name='Длительность процедуры')
    masters = models.ManyToManyField('old.OldPerson', blank=True, verbose_name='Специалисты',
                                     related_name='services',
                                     through='old.OldServiceMaster',
                                     through_fields=('service', 'master'))
    comment = models.TextField(null=True, blank=True, verbose_name='Комментарий')

    class Meta:
        managed = False
        db_table = 'deal_service'
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'
        ordering = ['group', 'name']
        default_permissions = ()

    def __str__(self):
        return self.name
        # return '%s: %s' % (self.group.name, self.name)


class OldServiceMaster(models.Model):
    """
        Оказываемая специалистом услуга
    """
    service = models.ForeignKey(OldService, related_name='related_masters', verbose_name='Услуга',
                                on_delete=models.CASCADE)
    master = models.ForeignKey('old.OldPerson', related_name='related_services', verbose_name='Специалист',
                               on_delete=models.CASCADE)
    cost = models.DecimalField(max_digits=30, decimal_places=2, verbose_name='Стоимость',
                               null=True, blank=True)
    cost_kid = models.DecimalField(max_digits=30, decimal_places=2, verbose_name='Стоимость (ребенок)',
                                   null=True, blank=True)
    diagnostic = models.DecimalField(max_digits=30, decimal_places=2, verbose_name='Стоимость диагностики',
                                     default=Decimal('0.00'))
    time = models.TimeField(null=True, blank=True, verbose_name='Длительность процедуры')

    reward = models.DecimalField(max_digits=30, decimal_places=2, verbose_name='Вознаграждение',
                                null=True, blank=True)
    reward_percent = models.BooleanField(default=False, verbose_name='Вознаграждение: процент')

    class Meta:
        managed = False
        db_table = 'deal_servicemaster'
        verbose_name = 'Оказываемая специалистом услуга'
        verbose_name_plural = 'Оказываемые специалистом услуги'
        unique_together = ('service', 'master')
        ordering = []
        default_permissions = ()

    def __str__(self):
        return '%s %s' % (self.service.__str__(), self.master.__str__())
