from datetime import datetime, time

from django.db import models


class OldBranch(models.Model):
    """
        Филиал
    """
    city = models.ForeignKey('old.OldCity', on_delete=models.CASCADE, verbose_name='Город')
    name = models.CharField(max_length=128, verbose_name='Наименование')
    phone = models.CharField(max_length=32, null=True, blank=True, verbose_name='Телефон')
    address = models.TextField(null=True, blank=True, verbose_name='Адрес')
    managers = models.ManyToManyField('old.OldPerson', blank=True, verbose_name='Администраторы',
                                      related_name='manager_branches',
                                      through='old.OldBranchManager', through_fields=('branch', 'user')
                                      )
    workers = models.ManyToManyField('old.OldPerson', blank=True, verbose_name='Персонал',
                                     related_name='worker_branches',
                                     through='old.OldBranchWorker', through_fields=('branch', 'user')
                                     )
    start_time = models.TimeField(default=time(10, 00), null=True, verbose_name='Начало рабочего дня')
    end_time = models.TimeField(default=time(20, 00), null=True, verbose_name='Конец рабочего дня')
    interval = models.TimeField(default=time(00, 15), null=True, blank=True, verbose_name='Интервал')
    periodic = models.BooleanField(default=True, verbose_name='Периодический')
    position = models.SmallIntegerField(default=0, blank=True, verbose_name='Сортировка')
    is_active = models.BooleanField(default=True, verbose_name='Активный')
    cache = models.JSONField(default=dict)

    class Meta:
        managed = False
        db_table = 'company_branch'
        ordering = ['-position', 'city', 'name']

    def __str__(self):
        return self.cache.get('label')


class OldBranchManager(models.Model):
    branch = models.ForeignKey('old.OldBranch', on_delete=models.CASCADE)
    user = models.ForeignKey('old.OldPerson', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'company_branch_managers'


class OldBranchWorker(models.Model):
    branch = models.ForeignKey('old.OldBranch', on_delete=models.CASCADE)
    user = models.ForeignKey('old.OldPerson', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'company_branch_workers'
