from django.db import models


class OldCity(models.Model):
    """
        Город
    """
    name = models.CharField(max_length=124, unique=True, verbose_name='Наименование')
    short = models.CharField(max_length=8, null=True, blank=True, verbose_name='Сокращение')
    timezone = models.SmallIntegerField(default=0, blank=True, verbose_name='Часовой пояс')
    position = models.SmallIntegerField(default=0, blank=True, verbose_name='Сортировка')

    class Meta:
        managed = False
        db_table = 'directory_city'
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def __str__(self):
        return self.name
