from datetime import date, timedelta, datetime, time
from django.db import models

from config.settings import DATETIME_FORMAT, TIME_FORMAT


class OldTimeTable(models.Model):
    """
        Табель учета рабочего времени
    """
    branch = models.ForeignKey('old.OldBranch', related_name='timetables', verbose_name='Филиал',
                               on_delete=models.CASCADE)
    user = models.ForeignKey('old.OldPerson', on_delete=models.CASCADE, verbose_name='Работник')
    plan_start_datetime = models.DateTimeField(verbose_name='Плановое начало смены')
    plan_end_datetime = models.DateTimeField(verbose_name='Плановое окончание смены')
    fact_start_datetime = models.DateTimeField(null=True, blank=True, verbose_name='Фактическое начало смены')
    fact_end_datetime = models.DateTimeField(null=True, blank=True, verbose_name='Фактическое окончание смены')
    comment = models.TextField(default='', blank=True, verbose_name='Комментарий')
    cache = models.JSONField(default=dict)

    class Meta:
        managed = False
        db_table = 'company_timetable'
        verbose_name = 'Табель учета рабочего времени'
        verbose_name_plural = 'Табель учета рабочего времени'
        ordering = []
        default_permissions = ()
        permissions = []

    def __str__(self):
        _start = (self.fact_start_datetime or self.plan_start_datetime) + timedelta(hours=self.branch.city.timezone)
        _end = (self.fact_end_datetime or self.plan_end_datetime) + timedelta(hours=self.branch.city.timezone)
        return '{branch}, {user}: {day} {start}-{end}'.format(
            branch=self.branch.name,
            user=self.user,
            day=_start.strftime('%d.%m.%Y'),
            start=_start.strftime(TIME_FORMAT),
            end=_end.strftime(TIME_FORMAT),
        )


class OldTimeGroup(models.Model):
    """
        Группы для расписания
    """
    branch = models.ForeignKey('old.OldBranch', related_name='time_groups', verbose_name='Филиал',
                               on_delete=models.CASCADE)
    name = models.CharField(max_length=128, blank=True, verbose_name='Наименование')
    users = models.ManyToManyField(to='old.OldPerson', blank=True, verbose_name='Персонал',
                                   related_name='time_groups',
                                   through='old.OldTimeGroupUser',
                                   through_fields=('timegroup', 'user'))
    timeout = models.BooleanField(default=False, verbose_name='Перерыв')
    start_date = models.DateField(verbose_name='Начало периода (день)')
    end_date = models.DateField(verbose_name='Конец периода (день)')
    start_time = models.TimeField(verbose_name='Начало интервала (время)')
    end_time = models.TimeField(verbose_name='Конец интервала (время)')

    class Meta:
        managed = False
        db_table = 'company_timegroup'
        verbose_name = 'Группа в расписании'
        verbose_name_plural = 'Группы для расписания'
        ordering = ['branch', 'start_time', '-end_time', 'start_date', '-end_date']
        default_permissions = ()
        permissions = []

    def __str__(self):
        start_date, end_date = self.start_date.strftime('%d.%m.%Y'), self.end_date.strftime('%d.%m.%Y')
        start_time, end_time = self.start_time.strftime('%H:%M'), self.end_time.strftime('%H:%M')
        if self.name:
            return '%s: %s-%s (%s-%s)' % (self.name, start_date, end_date, start_time, end_time)
        return '%s-%s (%s-%s)' % (start_date, end_date, start_time, end_time)


class OldTimeGroupUser(models.Model):
    timegroup = models.ForeignKey('old.OldTimeGroup', on_delete=models.CASCADE)
    user = models.ForeignKey('old.OldPerson', on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'company_timegroup_users'
        default_permissions = ()
        permissions = []