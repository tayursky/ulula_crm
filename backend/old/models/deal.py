from django.db import models


class OldStage(models.Model):
    """
        Этапы сделок
    """

    name = models.CharField(max_length=32, null=True, blank=True, verbose_name='Метка')
    label = models.CharField(max_length=32, null=True, blank=True, verbose_name='Наименование')
    step = models.IntegerField(default=0, verbose_name='Последовательность')
    color = models.CharField(max_length=7, default='#000000', blank=True, verbose_name='Цвет текста')
    background_color = models.CharField(max_length=7, default='#ffffff', blank=True, verbose_name='Цвет фона')
    comment = models.TextField(null=True, blank=True, verbose_name='Комментарий')

    class Meta:
        managed = False
        db_table = 'deal_stage'
        ordering = ['step']

    def __str__(self):
        return '%s %s' % (self.step, self.label)


class OldDeal(models.Model):
    DEAL_STATUS = (
        ('in_work', 'В работе'),
        ('closed', 'Закрыта')
    )
    DEAL_STATUS_NAME = dict(
        in_work='В работе',
        reject='Закрыта с провалом',
        ok='Успешно завершена'
    )
    branch = models.ForeignKey('old.OldBranch', null=True, on_delete=models.PROTECT, verbose_name='Филиал')
    status = models.CharField(max_length=32, choices=DEAL_STATUS, default='in_work', verbose_name='Статус сделки')
    stage = models.ForeignKey(OldStage, null=True, on_delete=models.PROTECT, verbose_name='Этап')
    services = models.ManyToManyField('old.OldService', verbose_name='Услуги')

    cost = models.DecimalField(default='0.00', blank=True, max_digits=30, decimal_places=2, verbose_name='Стоимость')
    paid = models.DecimalField(default='0.00', blank=True, max_digits=30, decimal_places=2, verbose_name='Оплачено')
    paid_non_cash = models.DecimalField(default='0.00', blank=True, max_digits=30, decimal_places=2,
                                        verbose_name='Оплачено безналом')
    discount = models.BooleanField(default=False, verbose_name='Скидка')
    comment = models.TextField(blank=True, verbose_name='Комментарий')

    start_datetime = models.DateTimeField(null=True, blank=True, verbose_name='Начало сеанса')
    finish_datetime = models.DateTimeField(null=True, blank=True, verbose_name='Конец сеанса')

    manager = models.ForeignKey('old.OldPerson', null=True, verbose_name='Администратор',
                                on_delete=models.CASCADE, related_name='manager_deals')
    master = models.ForeignKey('old.OldPerson', null=True, verbose_name='Специалист',
                               on_delete=models.CASCADE, related_name='master_deals')

    persons = models.ManyToManyField('old.OldPerson', through='old.OldDealPerson',
                                     through_fields=('deal', 'person'), related_name='rel_persons')

    # mlm_agent = models.ForeignKey('mlm.Agent', null=True, blank=True, verbose_name='МЛМ-агент',
    #                               on_delete=models.PROTECT, related_name='deals')

    created_at = models.DateTimeField(auto_now_add=True, null=True, verbose_name='Время создания')
    cache = models.JSONField(default=dict)
    deleted = models.DateTimeField(null=True, verbose_name='deleted')

    class Meta:
        managed = False
        db_table = 'deal_deal'
        verbose_name = 'Сделка'
        verbose_name_plural = 'Сделки'
        ordering = ['-start_datetime']
        default_permissions = ()

    def __str__(self):
        return '%s %s' % (self.pk, self.stage)


class OldDealPerson(models.Model):
    """
        Связи сделок с персонами
    """
    deal = models.ForeignKey(OldDeal, related_name='rel_persons', verbose_name='Сделка',
                             on_delete=models.CASCADE)
    person = models.ForeignKey('old.OldPerson', related_name='rel_deals', verbose_name='Персона',
                               on_delete=models.CASCADE)
    primary = models.BooleanField(default=True, verbose_name='Основной')
    control = models.BooleanField(default=False, verbose_name='Контроль')
    diagnostic = models.BooleanField(default=False, verbose_name='Диагностика')

    class Meta:
        managed = False
        db_table = 'deal_dealperson'
        verbose_name = 'Привязка к персоне'
        verbose_name_plural = 'Привязки к персонам'
        ordering = ['-primary']
        default_permissions = ()


OldDeal.services.through.oldservice.field.column = 'service_id'
OldDeal.services.through.olddeal.field.column = 'deal_id'
