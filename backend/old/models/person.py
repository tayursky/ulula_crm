from django.db import models
from django.utils.translation import gettext_lazy as _


class OldGroup(models.Model):
    name = models.CharField(max_length=150, unique=True)

    class Meta:
        managed = False
        db_table = 'auth_group'


class OldAccountGroup(models.Model):
    group = models.ForeignKey('old.OldGroup', null=False, on_delete=models.CASCADE)
    account = models.ForeignKey('old.OldAccount', null=False, on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'identity_account_groups'


class OldAccount(models.Model):
    username = models.CharField(_('username'), max_length=150, unique=True)
    password = models.CharField(_('password'), max_length=256)
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False)
    is_active = models.BooleanField(_('active'), default=True)
    is_superuser = models.BooleanField(_('superuser'), default=False)
    groups = models.ManyToManyField('old.OldGroup', through='old.OldAccountGroup',
                                    through_fields=('account', 'group'), related_name='rel_persons')

    class Meta:
        managed = False
        db_table = 'identity_account'

    @property
    def person(self):
        return self.person or None


class OldPerson(models.Model):
    """
        Персональные данные
    """
    SEX = (
        ('', '--'), ('man', 'муж'), ('woman', 'жен'),
    )
    first_name = models.CharField(max_length=128, verbose_name='Имя')
    patronymic = models.CharField(max_length=128, blank=True, verbose_name='Отчество')
    last_name = models.CharField(max_length=128, verbose_name='Фамилия')
    sex = models.CharField(choices=SEX, max_length=5, null=True, default='', verbose_name='Пол')
    birthday = models.DateField(null=True, blank=True, verbose_name='День рождения')
    city = models.ForeignKey('directory.City', null=True, on_delete=models.PROTECT, verbose_name='Город')
    timezone = models.SmallIntegerField(null=True, blank=True, verbose_name='Часовой пояс')
    comment = models.TextField(blank=True, verbose_name='Комментарий')
    account = models.OneToOneField(OldAccount, null=True, blank=True, verbose_name='Аккаунт',
                                   on_delete=models.CASCADE, related_name='person')
    token = models.CharField(max_length=32, null=True, unique=True, verbose_name='Токен')
    sip_id = models.CharField(max_length=128, null=True, blank=True, verbose_name='Манго-офис')
    # mighty_call_user = models.ForeignKey('sip.MightyCallUser', null=True, blank=True, verbose_name='Яндекс телефония',
    #                                      on_delete=models.CASCADE, help_text='Пользователь яндекс телефонии')
    address = models.CharField(max_length=128, null=True, blank=True, verbose_name='Адрес регистрации')
    passport_number = models.CharField(max_length=32, null=True, blank=True, verbose_name='Паспорт серия и номер')
    passport_issued = models.CharField(max_length=128, null=True, blank=True, verbose_name='Выдавший орган')
    cache = models.JSONField(default=dict)

    class Meta:
        managed = False
        db_table = 'identity_person'
        verbose_name = 'Персона'
        verbose_name_plural = 'Персоны'
        ordering = ['last_name', 'first_name']

    def __str__(self):
        return self.get_full_name_display()

    def cache_update(self):
        self.cache.update(dict(
            full_name=self.get_full_name_display(),
            phone=self.get_phone()
        ))
        return True

    def get_full_name_display(self):
        return ('%s %s %s' % (self.last_name, self.first_name, self.patronymic)).strip()

    def get_short_name_display(self):
        return self.get_short_name(self.get_full_name_display())

    @staticmethod
    def get_short_name(full_name):
        value = full_name.split(' ')
        return ('%s %s' % (value[0], ''.join([i[0] + '.' for index, i in enumerate(value) if index > 0]))).title()

    def get_phone(self):
        try:
            return self.phones.first().value
        except AttributeError:
            return ''

    def get_birthday_display(self):
        return self.birthday.strftime('%d.%m.%Y')

    def get_email(self):
        try:
            return self.emails.first().value
        except AttributeError:
            return ''

    def get_contact(self):
        try:
            return self.contacts.first()
        except AttributeError:
            return ''


class OldPersonFamilyRelations(models.Model):
    """
        Семейные связи
    """
    RELATIVE_TYPE = (
        (0, 'отец'),
        (1, 'мать'),
    )
    person = models.ForeignKey(OldPerson, related_name='family', verbose_name='Родственные связи',
                               on_delete=models.CASCADE)
    relative_type = models.PositiveSmallIntegerField(choices=RELATIVE_TYPE, default=0, verbose_name='Отношения')
    relative = models.ForeignKey(OldPerson, related_name='relative', verbose_name='Родственник',
                                 on_delete=models.CASCADE)

    parent_rel = 'person'

    class Meta:
        managed = False
        db_table = 'identity_personfamilyrelations'
        verbose_name = 'Семейные связи'
        verbose_name_plural = 'Семейные связи'
        default_permissions = ()

    def __str__(self):
        return '%s - %s - %s' % (self.person, self.relative_type, self.relative)


class OldPersonEmail(models.Model):
    """
        E-mail
    """
    person = models.ForeignKey(OldPerson, on_delete=models.CASCADE, related_name='emails', verbose_name='Персона')
    value = models.EmailField(verbose_name='E-mail')
    comment = models.CharField(max_length=124, null=True, blank=True, verbose_name='Комментарий')

    class Meta:
        managed = False
        db_table = 'identity_personemail'
        verbose_name = 'E-mail'
        verbose_name_plural = 'E-mail'
        ordering = ['value']

    def __str__(self):
        return self.value


class OldPersonPhone(models.Model):
    """
        Телефон
    """
    PHONE_TYPE = (
        (1, 'сотовый'),
        (2, 'рабочий')
    )
    person = models.ForeignKey(OldPerson, on_delete=models.CASCADE, related_name='phones', verbose_name='Персона')
    type = models.PositiveSmallIntegerField(choices=PHONE_TYPE, default=1, verbose_name='Тип телефона')
    value = models.CharField(max_length=32, verbose_name='Телефон')
    comment = models.CharField(max_length=124, null=True, blank=True, verbose_name='Комментарий')

    class Meta:
        managed = False
        db_table = 'identity_personphone'
        verbose_name = 'Телефон'
        verbose_name_plural = 'Телефоны'
        ordering = ['type', 'value']

    def __str__(self):
        return self.value


class OldPersonContact(models.Model):
    """
        Дополнительные контакты
    """
    CONTACT_TYPE = (
        (0, ' '),
        (1, 'соц.сеть'),
        (2, 'мессенджер')
    )
    person = models.ForeignKey(OldPerson, on_delete=models.CASCADE, related_name='contacts', verbose_name='Персона')
    type = models.PositiveSmallIntegerField(choices=CONTACT_TYPE, default=0, verbose_name='Тип контакта')
    value = models.CharField(max_length=32, verbose_name='Доп. контакт')
    comment = models.CharField(max_length=124, null=True, blank=True, verbose_name='Комментарий')

    class Meta:
        managed = False
        db_table = 'identity_personcontact'
        verbose_name = 'Доп. контакт'
        verbose_name_plural = 'Доп. контакты'
        ordering = ['type', 'value']

    def __str__(self):
        return self.value
