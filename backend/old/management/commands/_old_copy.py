from datetime import timedelta

from django.contrib.auth.models import Group

from old.models import (
    OldCity, OldPerson, OldStage, OldDeal, OldBranch, OldServiceGroup, OldTimeTable, OldTimeGroup
)
from apps.appoint.models import Stage, Appoint, AppointPerson
from apps.company.models import Branch, BranchCrew, ServiceGroup, Service, ServiceExecutor, Timetable, TimetableGroup
from apps.directory.models import City
from identity.models import Person, Phone, Contact, User


class CopyData:

    @staticmethod
    def copy_person():
        print('\nCopy person...')
        Person.truncate()
        qs = OldPerson.objects.using('old').all().order_by('id')
        count = qs.count()
        for old in qs:
            person, _ = Person.objects.get_or_create(
                first_name=old.first_name,
                patronymic=old.patronymic,
                last_name=old.last_name,
                birthday=old.birthday
            )
            if old.account:
                user, _ = User.objects.get_or_create(username=old.account.username)
                user.password = old.account.password
                fields = ['email', 'is_staff', 'is_active', 'is_superuser']
                person.user = copy_fields(user, old.account, fields)
                if old.account.groups.filter(name='Правщики').exists():
                    group, _ = Group.objects.get_or_create(name='Специалисты')
                    person.user.groups.add(group)
            fields = ['sex', 'timezone', 'comment', 'address', 'passport_number', 'passport_issued', 'cache']
            for phone in old.phones.exclude(value__in=[None, '', ' ']):
                Contact.objects.create(person=person, type='phone', value=phone.value, comment=phone.comment)
            for email in old.emails.exclude(value__in=[None, '', ' ']):
                Contact.objects.create(person=person, type='email', value=email.value, comment=email.comment)
            person = copy_fields(person, old, fields)
            count -= 1
            print(count, person)

    @staticmethod
    def copy_appoint():
        print('\nCopy appoint...')
        old_deal = OldDeal.objects.using('old').filter(deleted=None).order_by('-start_datetime')
        # old_deal = old_deal.filter(branch=65, start_datetime__gte='2022-06-01')
        count = old_deal.count()
        Appoint.truncate()
        for old in old_deal:
            try:
                branch = Branch.objects.get(name=old.branch.name, city__name=old.branch.city.name)
            except AttributeError:
                branch = None
            appoint = Appoint.objects.create(
                branch=branch,
                status='in_work' if old.status == 'in_work' else 'done',
                stage=Stage.objects.get(name=old.stage.name),
            )
            fields = ['cost', 'paid', 'paid_non_cash', 'discount', 'comment',
                      ['start_datetime', 'begin'], ['finish_datetime', 'end']]
            appoint = copy_fields(appoint, old, fields)
            old_service_names = [s['name'] for s in old.services.all().values('name')]
            appoint.services.add(*[i for i in Service.objects.filter(name__in=old_service_names)])
            if old.master:
                AppointPerson.objects.create(appoint=appoint, person=get_person(old.master), role='staff')
            if old.manager:
                AppointPerson.objects.create(appoint=appoint, person=get_person(old.manager), role='manager')
            for rel_person in old.rel_persons.all():
                AppointPerson.objects.create(
                    role='client',
                    appoint=appoint, person=get_person(rel_person.person),
                    primary=rel_person.primary, control=rel_person.control, diagnostic=rel_person.diagnostic
                )
            count -= 1
            print(count, appoint)
            appoint.save()  # for updating cache

    @staticmethod
    def copy_city():
        print('\nCopy city...')
        for old in OldCity.objects.using('old').all():
            city, _ = City.objects.get_or_create(name=old.name)
            city.timezone = old.timezone
            city.save()
            print(city)

    @staticmethod
    def copy_branch():
        print('\nCopy branch...')
        for old in OldBranch.objects.using('old').all():
            city = City.objects.get(name=old.city.name)
            branch, _ = Branch.objects.get_or_create(city=city, name=old.name)
            persons = []
            for user in old.managers.all():
                person = get_person(user)
                if person:
                    persons.append(dict(branch_id=branch.id, person_id=person.id, role='manager'))
            for user in old.workers.all():
                person = get_person(user)
                if person:
                    persons.append(dict(branch_id=branch.id, person_id=person.id, role='staff'))
            BranchCrew.objects.filter(branch=branch).delete()
            BranchCrew.objects.bulk_create([BranchCrew(**i) for i in persons])
            fields = ['name', 'phone', 'address', ['start_time', 'begin_time'], 'end_time', 'is_active', 'cache']
            res = copy_fields(branch, old, fields)
            print(res)

    @staticmethod
    def copy_service():
        print('\nCopy service...')
        for old_group in OldServiceGroup.objects.using('old').all():
            group, _ = ServiceGroup.objects.get_or_create(name=old_group.name, comment=old_group.comment)
            print(group)
            for old_service in old_group.services.all():
                service, _ = Service.objects.get_or_create(group=group, name=old_service.name)
                fields = ['cost', 'cost_kid', 'time', 'comment']
                service = copy_fields(service, old_service, fields)
                print(service)
                for service_master in old_service.related_masters.all():
                    service_staff = ServiceExecutor.objects.get_or_create(
                        service=service,
                        staff=get_person(service_master.master),
                        cost=service_master.cost,
                        cost_kid=service_master.cost_kid,
                        time=service_master.time
                    )
                    print(service_staff)

    @staticmethod
    def copy_stage():
        print('\nCopy stage...')
        for old in OldStage.objects.using('old').all():
            stage, _ = Stage.objects.get_or_create(name=old.name)
            fields = ['name', 'label', 'step', 'color', 'background_color', 'comment']
            for field in fields:
                setattr(stage, field, getattr(old, field))
                stage.save()
            print(stage)

    @staticmethod
    def copy_timetable():
        print('\nCopy timetable...')
        old_data = OldTimeTable.objects.using('old').all()
        total, cnt = old_data.count(), 1
        Timetable.truncate()
        for old in old_data:
            branch = Branch.objects.get(name=old.branch.name, city__name=old.branch.city.name)
            timetable = Timetable.objects.create(
                branch=branch,
                staff=get_person(old.user),
                begin=old.plan_start_datetime,
                end=old.plan_end_datetime,
                begin_really=old.fact_start_datetime,
                end_really=old.fact_end_datetime,
                comment=old.comment
            )
            print(total, '/', cnt, timetable)
            cnt += 1

    @staticmethod
    def copy_timetablegroup():
        print('\nCopy timetablegroup...')
        old_data = OldTimeGroup.objects.using('old').all()
        total, cnt = old_data.count(), 1
        TimetableGroup.truncate()
        for old in old_data:
            data = dict(
                branch=Branch.objects.get(name=old.branch.name, city__name=old.branch.city.name),
                begin_date=old.start_date,
                begin_time=old.start_time
            )
            for field in ['name', 'timeout', 'end_date', 'end_time']:
                data[field] = getattr(old, field)
            timetable_group = TimetableGroup.objects.create(**data)

            staffs = []
            for user in old.users.all():
                person = get_person(user)
                if person:
                    staffs.append(person.id)
            timetable_group.staffs.add(*staffs)
            timetable_group.save()
            print(total, '/', cnt, timetable_group)
            cnt += 1


def copy_fields(new, old, fields):
    for field in fields:
        if isinstance(field, list):
            # field[0] - старое имя, field[1] - новое имя поля
            setattr(new, field[1], getattr(old, field[0]))
        else:
            setattr(new, field, getattr(old, field))
    new.save()
    return new


def get_person(old):
    try:
        return Person.objects.get(
            first_name=old.first_name,
            patronymic=old.patronymic,
            last_name=old.last_name,
            birthday=old.birthday
        )
    except AttributeError:
        return None
