from django.core.management.base import BaseCommand, CommandError

from ._old_copy import CopyData


class Command(BaseCommand):
    help = 'Parser from old database'

    def add_arguments(self, parser):
        parser.add_argument(
            '-db',
            '--databases',
            default=[
                'person', 'city', 'branch', 'stage', 'service', 'appoint', 'timetable', 'timetablegroup'
            ],
            help='Databases list',
        )

    def handle(self, *args, **options):
        db = options['databases']
        copy_data = CopyData()
        if isinstance(db, list):
            for item in db:
                getattr(copy_data, 'copy_%s' % item)()
        else:
            getattr(copy_data, 'copy_%s' % db)()
