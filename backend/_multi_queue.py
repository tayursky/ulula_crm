import multiprocessing
import random

from pubmed.parser.proxy_master import ProxyMaster

urls = [f'url_{str(i).zfill(3)}' for i in range(20)]
proxy_master = ProxyMaster(5)


def check(url):
    proxy = proxy_master.get()
    wait = random.randint(1, 5)
    # time.sleep(wait)
    print(f'+{url}-{wait} {proxy}+')
    proxy_master.free(proxy)


queue = multiprocessing.Queue
pr = multiprocessing.Process(target=check, args=(queue,))

print(proxy_master.proxies)
