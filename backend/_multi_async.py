import time
import random
import asyncio
import threading
from threading import BoundedSemaphore

# from pubmed.parser.proxy_master import ProxyMaster

from multiprocessing.pool import ThreadPool

lst = [i for i in range(30)]


def core_check(line):
    wait = random.randint(1, 5)
    time.sleep(wait)
    print(f'+{line}-{wait}+')


def main(lst):
    with ThreadPool(10) as p:
        p.map(core_check, lst)
    p.close()
    p.join()


main(lst)