import axios from 'axios'
// import store from '../store'
import router from '@/router'

const baseURL = 'http://localhost:8000/api/'
const token = localStorage.getItem('token') || null
// console.log('token', token)

const app = axios.create({
    baseURL,
    headers: {
        'Authorization': token ? `Bearer ${token}` : '',
        // 'Referrer-Policy': 'strict-origin-when-cross-origin'
    }
});

app.interceptors.request.use(
    config => {
        // console.log('request', config)
        return config
    },
    error => {
        return Promise.reject(error)
    }
)

app.interceptors.response.use(
    response => {
        // console.log('response', response)
        // console.log(store)
        // store.commit('commonSetTest', response.data.day)
        // console.log(store.state.common.test)
        return response
    },
    error => {
        console.log('response error', error.response)
        if(error.response.statusText === 'Unauthorized') {
            console.log('Unauthorized')
            router.push('/login')
        }
        return Promise.reject(error)
    }
)

const multipart = axios.create({
    baseURL,
    headers: {
        'Authorization': token ? `Bearer ${token}` : '',
        'Content-Type': 'multipart/form-data'
    }
});

export default {
    app,
    multipart,
}