import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import {createPinia} from 'pinia'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import vuetify from './plugins/vuetify'

// import moment from 'moment'
// import localization from 'moment/locale/ru'
// moment.updateLocale('ru', localization)

import DatePicker from 'vue-datepicker-next'
import 'vue-datepicker-next/index.css'
import 'vue-datepicker-next/locale/ru'

// import { loadFonts } from './plugins/webfontloader'
// loadFonts()


const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
app.use(createPinia())
  .use(router)
  .use(ElementPlus)
  .use(vuetify)
  .use(DatePicker)
  // .component('VueDatePicker', VueDatePicker)
  .mount('#app')
