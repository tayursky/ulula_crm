import {defineStore} from 'pinia'
import api from '@/plugins/api.js'
import {useAppointMonthStore} from '@/stores/appoint_month.js'
import moment from "moment/moment";

export const useAppointWeekStore = defineStore('appointWeek', {
  state: () => ({
    settings: {
      cell_height: 26,
      range: {label: 'Дней', current: 7, items: [1, 2, 3, 5, 7]},
      minutes: {label: 'Сетка', current: 15, items: [5, 10, 15, 20, 30]},
    },
    day: null,
    range: null,
    weekLabel: '',
    appoints: [],
    timesheet: {},
    timetable: [],
    weekDays: [],
    stages: [],
    related: {},
    filters: [],
    filtersData: {},
    initializing: false,
    loading: false,
    errors: {}
  }),

  actions: {
    reset() {
      this.weekLabel = ''
      this.appoints = []
      this.timesheet = {}
      this.timetable = []
    },

    init() {
      this.initializing = true
      this.errors = {}
      api.app.get('/appoint/week/case/')
        .then((response) => {
          this.filters = response.data.filters
        })
        .catch((errors) => {
          this.errors = errors.response.data
        })
        .finally(() => {
          this.initializing = false
          this.getWeek()
        })
    },

    getWeek(step = null) {
      this.reset()
      if (step === 'current') {
        this.day = null
        step = null
      }
      const params = {params: {day: this.day, range: this.range, step, ...this.filtersData}}
      api.app.get('appoint/week/', params)
        .then((response) => {
          if (response.data.day) {
            useAppointMonthStore().reset()
            useAppointMonthStore().month = response.data.day.slice(0, 7)
          }
          // this.storeMonth.month = response.day.slice(0, 7)
          this.filtersData.branch = response.data.branch
          this.appoints = response.data.appoints.map(appoint => {
            return {...appoint, begin_date: new Date(appoint.begin), end_date: new Date(appoint.end)}
          })
          this.day = response.data.day
          this.timesheet = response.data.timesheet
          this.weekDays = response.data.timesheet.weekdays
          this.weekLabel = response.data.week_label
          this.related = response.data.related
          this.stages = response.data.stages
          this.timetable = this.pushCells(response.data.timetable)
          this.loading = false
        })
    },

    pushCells(timetable) {
      return timetable.map((i) => {
        return {...i, cells: this.getCells(i)}
      })
    },

    getCells(timetable) {
      // Расписание на день с учетом специалиста
      const appoints = this.appoints.filter(appoint => {
        return appoint.cache.staff?.includes(timetable.staff)
          && appoint.begin.slice(0, 10) === timetable.begin.slice(0, 10)
      })
      let points = [] // массив пустых ячеек
      let cells = [] // результирующий массив с расписанием и пустыми ячейками
      let cellsFinish = []
      let begin_date = new Date(timetable.begin) // начало рабочего дня
      let appointLastEnd = begin_date
      // Собираем массив пустых ячеек с шагом this.settings.minutes.current
      while (begin_date < new Date(timetable.end)) {
        let end_date = moment(begin_date).add(this.settings.minutes.current, 'minutes').toDate()
        points.push({begin_date, end_date})
        begin_date = end_date
      }
      // Конкатенируем appoints и points
      for (const appoint of appoints) {
        let count = 0
        for (const point of points) {
          if (point.begin_date < appoint.begin_date) {
            count++
            let _begin_date = point.begin_date > appointLastEnd ? point.begin_date : appointLastEnd
            let _end_date = point.end_date > appoint.begin_date ? appoint.begin_date : point.end_date
            if (moment(_begin_date).format('HH:mm') !== moment(_end_date).format('HH:mm')) {
              cells.push(this.getEmptyCell(timetable, _begin_date, _end_date))
            }
          } else if (point.end_date <= appoint.end_date) count++
        }
        appoint.begin_hhmm = moment(appoint.begin_date).format('HH:mm')
        appoint.end_hhmm = moment(appoint.end_date).format('HH:mm')
        appoint.minutes = (appoint.end_date - appoint.begin_date) / 60000
        points.splice(0, count)
        cells.push(appoint)
        appointLastEnd = appoint.end_date
      }
      // Добавляем оставшиеся пустые ячейки к общему массиву
      for (const point of points) {
        cells.push(this.getEmptyCell(timetable, point.begin_date, point.end_date))
      }
      // Заполняем пробелы в расписании пустыми ячейками
      for (let index = 0; index < cells.length; index++) {
        let cell = cells[index]
        let cellNext = cells[index + 1]
        cellsFinish.push(cell)
        if (cellNext && cell.end_date < cellNext.begin_date) {
          cellsFinish.push(this.getEmptyCell(timetable, cell.end_date, cellNext.begin_date))
        }
      }
      return cellsFinish
    },

    getEmptyCell(timetable, begin_date, end_date) {
      return {
        branch: timetable.branch,
        staff: timetable.staff,
        begin: moment(begin_date).format('YYYY-MM-DDTHH:mm:ss'),
        begin_hhmm: moment(begin_date).format('HH:mm'),
        end: moment(end_date).format('YYYY-MM-DDTHH:mm:ss'),
        end_hhmm: moment(end_date).format('HH:mm'),
        minutes: (end_date - begin_date) / 60000
      }
    },

  },
})

