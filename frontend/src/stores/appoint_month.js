import {defineStore} from 'pinia'
import api from '@/plugins/api.js'

export const useAppointMonthStore = defineStore('appointMonth', {
  state: () => ({
    day: null,
    month: null,
    monthName: '',
    appoints: {},
    branches: {},
    timesheet: {},
    timetable: [],
    weekDays: [],
    stages: [],
    filters: [],
    filtersData: {},
    initializing: false,
    loading: false,
    errors: {}
  }),

  actions: {
    init() {
      this.initializing = true
      this.errors = {}
      api.app.get('/appoint/month/case/')
        .then((response) => {
          this.filters = response.data.filters
        })
        .catch((errors) => {
          this.errors = errors.response.data
        })
        .finally(() => {
          this.initializing = false
          this.getMonth()
        })
    },

    getMonth(step = null) {
      if (step === 'current') {
        this.month = null
        step = null
      }
      this.loading = true
      this.errors = {}
      const params = {params: {month: this.month, step, ...this.filtersData}}
      api.app.get('appoint/month/', params)
        .then((response) => {
          this.month = response.data.month
          this.monthName = response.data.month_name
          this.appoints = response.data.appoints
          this.branches = response.data.branches
          this.timesheet = response.data.timesheet
          this.stages = response.data.stages
          this.loading = false
        })
        .catch((errors) => {
          this.errors = errors.response.data
        })
    },

    reset() {
      this.monthName = ''
      this.appoints = {}
      this.timesheet = {}
      this.timetable = []
    }
  }
})

