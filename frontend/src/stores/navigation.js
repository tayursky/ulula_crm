import { defineStore } from 'pinia'
import router from '@/router/index.js'

export const useNavigationStore = defineStore('navigation', {
  state: () => ({
    title: 'Ulula-CRM',
    day: null,
    root: null,
    loading: false,
    items: {
      'left': [
        {
          name: 'appoint',
          children: [
            ['appoint_month', 'appoint_week'],
            ['appoint__kanban', 'appoint__list', 'appoint__client', 'appoint__task'],
            ['appoint__report', 'appoint__expense']
          ],
        }, {
          name: 'directory',
          children: [
            ['directory_city', 'directory_working_calendar'],
            ['company_service_group', 'company_service', 'company_service_executor', 'appoint_stage']
          ]
        }, {
          name: 'company',
          children: [
            ['company_branch', 'company_staff', 'company_timetable']
          ]
        }, {
          name: 'about',
        }, {
          name: 'test',
        },
      ],
      'right': [
        {
          name: 'account',
          children: [
            ['logout']
          ]
        }
      ]
    },
    sideBar: {
      mode: 'full', // or 'min', 'hide'
      width: 220, // or 48
    }
  }),

  getters: {

    getTopBarItems: (state, getters) => (key) => {
      let data = []
      state.items[key].forEach(item => {
        let _item = Object.assign({}, item)
        if (typeof item === 'object') {
          _item = Object.assign(_item, router.resolve({name: item.name}))
          // _item.title = _item.title === '$username' ? profile.username : _item.title
          if ('children' in _item) {
            _item.children = _item.children.map(children => {
              if (Array.isArray(children)) {
                return children.map(i => router.resolve({name: i}))
              }
            })
          }
        }
        data.push(_item)
      })
      return data
    },

    getSideBarItems: (state) => (root) => {
      const name = root || state.items.left[0].name
      const item = state.items.left.find(item => item.name === name)
      return (item && item.children)
        ? item.children.map(item => item.map(i => router.resolve({name: i})))
        : []
    },

    getSideBar: state => {
      return {
        mode: state.sideBar.mode,
        width: state.sideBar.width
      }
    },

  },

  actions: {
    showMessage({commit}, message) {
      commit('setMessage', {type: 'error', ...message})
    },

    switchSideBar() {
      this.sideBar.mode = this.sideBar.mode === 'full' ? 'min' : 'full'
      this.sideBar.width = this.sideBar.mode === 'full' ? 220 : 48
    }
  }
})
