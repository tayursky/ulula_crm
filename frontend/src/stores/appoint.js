import {defineStore} from 'pinia'
import api from '@/plugins/api.js'
import {useAppointWeekStore} from '@/stores/appoint_week.js'

export const useAppointStore = defineStore('appoint', {
  state: () => ({
    appoint: {},
    appointForm: [],
    personForm: [],
    contactForm: [],
    initializing: false,
    loading: false,
    errors: {}
  }),

  actions: {
    getForm() {
      this.initializing = true
      this.errors = {}
      api.app.get('/appoint/form/')
        .then((response) => {
          this.appointForm = response.data.appoint_form
          this.personForm = response.data.person_form
          this.contactForm = response.data.contact_form
        })
        .catch((errors) => {
          this.errors = errors.response.data
        })
        .finally(() => {
          this.initializing = false
        })
    },

    getAppoint() {
      this.loading = true
      this.errors = {}
      const params = {params: {}}
      api.app.get(`appoint/${this.appoint.id}/`, params)
        .then((response) => {
          this.appoint = response.data
          this.loading = false
        })
        .catch((errors) => {
          this.errors = errors.response.data
        })
    },

    saveItem() {
      this.loading = true
      this.errors = {}
      const _api = this.appoint.id
        ? api.app.put(`appoint/${this.appoint.id}/`, this.appoint)
        : api.app.post('appoint/', this.appoint)
      _api
        .then((response) => {
          // this.appoint = {}
        })
        .catch((errors) => {
          this.errors = errors.response.data
        })
        .finally(() => {
          this.loading = false
          // useAppointWeekStore().getWeek()
        })
    },

    delete() {
      this.loading = true
      this.errors = {}
      api.app.delete(`appoint/${this.appoint.id}/`)
        .then((response) => {
          this.appoint = {}
        })
        .catch((errors) => {
          this.errors = errors.response.data
        })
        .finally(() => {
          this.loading = false
          useAppointWeekStore().getWeek()
        })
    },

    escape() {
      console.log('escape')
      this.appoint = {}
    }
  }
})

