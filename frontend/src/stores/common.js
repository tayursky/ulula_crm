import {defineStore} from 'pinia'
import api from '@/plugins/api.js'

export const useCommonStore = defineStore('common', {
  state: () => ({
    initializing: false,
    url: '',
    model_label: undefined,
    model_name: '',
    verbose_name: '',
    verbose_name_plural: '',
    headers: [],
    form: {},

    items: [],
    itemsCount: 0,
    itemsLoading: false,
    item: {},
    itemLoading: false,
    itemErrors: {},

    ordering_fields: [],
    ordering: null,
    pagination: {
      page: 1, size: 10, length: 0
    },
    errors: {},
  }),

  actions: {
    async init(params) {
      this.initializing = true
      this.errors = {}
      this.pagination.page = 1
      this.model_label = params.model_label
      if (this.model_label === undefined) return
      this.url = `/common/model/${this.model_label}/`
      await api.app.get(`${this.url}case/`)
        .then((response) => {
          this.model_name = response.data.model_name
          this.verbose_name = response.data.verbose_name
          this.verbose_name_plural = response.data.verbose_name_plural
          this.headers = response.data.headers
          this.form = response.data.form
          this.ordering_fields = response.data.ordering_fields
          this.ordering = response.data.ordering
        })
        .catch((errors) => {
          this.errors = errors.response.data
        })
        .finally(() => {
          this.initializing = false
          this.getItems()
        })
    },

    async getItems() {
      this.items = {}
      this.itemsCount = 0
      this.itemsLoading = true
      const params = {
        ordering: this.ordering,
        page: this.pagination.page > 1 ? this.pagination.page : null
      }
      await api.app.get(this.url, {params})
        .then((response) => {
          this.itemsCount = response.data.count
          this.items = response.data.results
          this.setPagination()
        })
        .catch((errors) => {
          this.errors = errors.response.data
        })
        .finally(() => {
          this.itemsLoading = false
        })
    },

    async getItem(item) {
      this.itemLoading = true
      this.itemErrors = {}
      await api.app.get(`${this.url}${item.id}/`)
        .then((response) => {
          this.item = response.data
        })
        .catch((errors) => {
          this.itemErrors = errors.response.data
        })
        .finally(() => {
          this.itemLoading = false
        })
    },

    async saveItem() {
      this.itemLoading = true
      this.itemErrors = {}
      const _api = this.item.id
        ? api.app.put(`${this.url}${this.item.id}/`, this.item)
        : api.app.post(this.url, this.item)
      await _api
        .then((response) => {
          this.item = {}
          this.getItems()
        })
        .catch((errors) => {
          this.itemErrors = errors.response.data
        })
        .finally(() => {
          this.itemLoading = false
        })
    },

    async addItem() {
      this.itemErrors = {}
      this.item = this.form.reduce((sum, field) => {
        if (field.name !== 'id') {
          return {...sum, [field.name]: field.default}
        }
      }, {})
    },

    async deleteItem() {
      this.itemLoading = true
      this.itemErrors = {}
      await api.app.delete(`${this.url}${this.item.id}/`)
        .then((response) => {
          this.item = {}
          this.getItems()
        })
        .catch((errors) => {
          this.itemErrors = errors.response.data
        })
        .finally(() => {
          this.itemLoading = false
        })
    },

    setPagination() {
      this.pagination.length = Math.ceil(this.itemsCount / this.pagination.size)
    }
  },
})

