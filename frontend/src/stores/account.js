import { defineStore } from 'pinia'
import api from '@/plugins/api.js'

export const useAccountStore = defineStore('account', {
  state: () => ({
    account: {},
    token: null,
    settings: {},
    permissions: [],
    errors: {}
  }),

  getters: {
    profile: state => state.profile,
  },

  actions: {
    async login(params) {
      this.errors = {}
      const account = await api.app.post('identity/auth/login/', params)
        .then((response) => {
          if (!response.data.token) throw response
          // this.account = account
          this.setAccessToken(response.data)
          return response
        })
        .catch((errors) => {
          console.log('errors', errors)
          this.errors = errors.response.data
        })
    },

    setAccessToken(data) {
      if (data === undefined || data === null) {
        localStorage.removeItem('token')
        this.account = {}
      } else {
        api.app.defaults.headers['Authorization'] = `Bearer ${data.token}`
        localStorage.token = data.token
        this.account = {}
      }
    },

    setState(data) {
      this[data[0]] = data[1]
    },

  },
})

