import List from '@/components/common/List.vue'

export default {
  routes: [
    {
      name: 'company_service_group',
      path: 'service_group',
      component: List,
      meta: {
        title: 'Группы услуг',
        model_label: 'service_group',
        icon: 'mdi-group',
      }
    }, {
      name: 'company_service',
      path: 'service',
      component: List,
      meta: {
        title: 'Услуги',
        model_label: 'service',
        icon: 'mdi-playlist-edit',
      }
    }, {
      name: 'company_service_executor',
      path: 'service_executor',
      component: List,
      meta: {
        title: 'Оказываемые специалистами услуги',
        model_label: 'service_executor',
        icon: 'mdi-account-details',
      }
    }, {
      name: 'appoint_stage',
      path: 'stage',
      component: List,
      meta: {
        title: 'Этапы сделок',
        model_label: 'stage',
        icon: 'mdi-counter',
      }
    }, {
      name: 'directory_city',
      path: 'city',
      component: List,
      meta: {
        title: 'Города',
        icon: 'mdi-city',
        model_label: 'city',
      }
    }, {
      name: 'directory_working_calendar',
      path: 'working_calendar',
      component: () => import('@/views/directory/WorkingCalendar.vue'),
      meta: {
        title: 'Производственный календарь',
        icon: 'mdi-calendar-star',
        url: '/api/directory/working_calendar/',
      }
    },
  ]
}