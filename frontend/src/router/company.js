import List from '@/components/common/List.vue'
import Test from '@/views/Test.vue'

export default {
  routes: [
    {
      name: 'company_branch',
      path: 'branch',
      component: List,
      meta: {
        title: 'Филиалы',
        model_label: 'branch',
        icon: 'mdi-flag',
      }
    }, {
      name: 'company_staff',
      path: 'staff',
      component: List,
      meta: {
        title: 'Сотрудники',
        model_label: 'staff',
        icon: 'mdi-account-multiple',
      }
    }, {
      name: 'company_timetable',
      path: 'timetable',
      component: Test,
      meta: {
        title: 'Табель',
        model_label: 'timetable',
        icon: 'mdi-timetable',
        url: '/api/company/timetable/',
      }
    },
  ]
}