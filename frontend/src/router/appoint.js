// const Test = defineAsyncComponent(() => import('@/views/Test.vue'))
import Test from '@/views/Test.vue'

export default {
  routes: [
    {
      name: 'appoint_month',
      path: 'month',
      component: () => import('@/views/appoint/AppointMonth.vue'),
      meta: {
        title: 'Расписание на месяц',
        icon: 'mdi-calendar-month',
        api: '/api/appoint/month/'
      }
    }, {
      name: 'appoint_week',
      path: 'week',
      // component: Test,
      component: () => import('@/views/appoint/AppointWeek.vue'),
      meta: {
        title: 'Расписание на неделю',
        icon: 'mdi-calendar-week',
        api: '/api/appoint/week/'
      }
    }, {
      name: 'appoint__kanban',
      path: 'kanban',
      component: Test,
      meta: {
        title: 'Канбан',
        icon: 'mdi-view-column',
        api: '/api/appoint/kanban/'
      }
    }, {
      name: 'appoint__list',
      path: 'list',
      component: Test,
      meta: {
        title: 'Список приемов',
        icon: 'mdi-format-list-checks',
        api: '/api/appoint/list/'
      }
    }, {
      name: 'appoint__client',
      path: 'client',
      component: Test,
      meta: {
        title: 'Клиенты',
        icon: 'mdi-account-box-multiple',
        api: '/api/appoint/client/'
      }
    }, {
      name: 'appoint__task',
      path: 'task',
      component: Test,
      meta: {
        title: 'Задачи',
        icon: 'mdi-check-box-multiple-outline',
        api: '/api/appoint/task/'
      }
    }, {
      name: 'appoint__report',
      path: 'report',
      component: Test,
      meta: {
        title: 'Отчеты',
        icon: 'mdi-clipboard-arrow-up-outline',
        api: '/api/appoint/report/'
      }
    }, {
      name: 'appoint__expense',
      path: 'expense',
      component: Test,
      meta: {
        title: 'Расходы',
        icon: 'mdi-circle-slice-2',
        api: '/api/appoint/expense/'
      }
    },
  ]
}