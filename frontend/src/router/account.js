// const Test = defineAsyncComponent(() => import('@/views/Test.vue'))

export default {
  routes: [
    {
      name: 'login',
      path: '/login',
      component: () => import('@/views/account/Login.vue'),
      meta: {
        title: 'Вход',
        icon: 'mdi-login',
        layout: 'empty'
      }
    }, {
      name: 'logout',
      path: '/logout',
      component: () => import('@/views/account/Logout.vue'),
      meta: {
        title: 'Выход',
        icon: 'mdi-logout',
        layout: 'empty'
      }
    },
  ]
}