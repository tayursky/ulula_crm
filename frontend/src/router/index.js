import { createRouter, createWebHistory } from 'vue-router'

import account from '@/router/account.js'
import appoint from '@/router/appoint.js'
import company from '@/router/company.js'
import directory from '@/router/directory.js'

let routes = [
  {
    name: 'home',
    path: '/',
    component: () => import('@/views/Test.vue'),
    meta: {
      title: 'Домой',
      icon: 'mdi-calendar-week',
    }
  },
  {
    name: 'account',
    path: '/account',
    children: account.routes,
    meta: {
      title: '$username',
      icon: 'mdi-account'
    }
  },
  {
    name: 'appoint',
    path: '/appoint',
    children: appoint.routes,
    // component: () => import('@/views/appoint'),
    meta: {
      title: 'Расписание',
      icon: 'mdi-progress-clock',
    }
  },
  {
    name: 'directory',
    path: '/directory',
    children: directory.routes,
    meta: {
      title: 'Словарь',
      icon: 'mdi-ballot',
    }
  },
  {
    name: 'company',
    path: '/company',
    children: company.routes,
    meta: {
      title: 'Компания',
      icon: 'mdi-home',
    }
  },
  {
    name: 'about',
    path: '/about',
    component: () => import('@/components/HelloWorld.vue'),
    meta: {
      title: 'О проекте',
      icon: 'mdi-calendar-week',
    }
  },
  {
    name: 'test',
    path: '/test',
    component: () => import('@/views/AboutView.vue'),
    meta: {
      title: 'Тест',
      icon: 'mdi-calendar-week',
    }
  },
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  base: import.meta.env.BASE_URL,
  // base: process.env.BASE_URL,
  routes
})

export default router
